## PAOHVis (Parallel Aggregated Ordered Hypergraph Visualization) 

Parallel Aggregated Ordered Hypergraph (PAOH) is a novel technique to visualize dynamic hypergraphs. Hypergraphs are a generalization of graphs where edges can connect more than two vertices. Hypergraphs can be used to model co-authorship networks with multiple authors per article, or networks of business partners. A dynamic hypergraph evolves over discrete time slots. A PAOH display represents vertices as parallel horizontal bars and hyperedges as vertical lines that connect two or more vertices.

Please go to <https://aviz.fr/paohvis> for more information or contact us: <paohvis@inria.fr>.

## Run Paohvis

Paohvis is implemented using [Dart](https://dart.dev). You can run Paohvis locally:

1. Clone this git
2. Install Dart if it is not already installed. See instructions here: `https://dart.dev/get-dart`
3. `cd paohvis`
4. `pub get`
5. `webdev serve` or `~/.pub-cache/bin/webdev` depending on your operating system
6. Go to: http://localhost:8080/

## Info to deploy Paohvis
In order to deploy the app to a web server:

* on MacOS right-click on `pubspec.yaml`, then select Pub:Build... A folder `build/web` will be generated and is the folder to upload on the server
* on Linux and Windows, run `webdev build`. A folder `build/` will be generated and is the folder to upload on the server
