part of paohvis;

class TsTabs {

  CanvasElement _canvas;
  CanvasRenderingContext2D _context;
  Graph _graph;

  TimeManager tm = new TimeManager();
  num _topMargin = 0;
  num _bottomMargin = 5;
  num _fontSize = MAX_FONT;
  num _tabMargin = 4;
  num _height = 0;
  num _width = 0;
  num _statsHeight = 0;
  num _minWidth = ((MAX_FONT + 5)/2) + 5;
  num _leftMarginCheckbox = 5;


  TsTabs(Graph g, CanvasElement canvasElement, [num yv = 0, num width, num statsHeight]) {
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    _topMargin = yv;
    _height = _fontSize + _bottomMargin;
    _width = width;
    _graph = g;
    _statsHeight = statsHeight;

    if(DEMO_VIS4DH){
      _fontSize = MAX_FONT;
    }
  }

  int get numTimes  => tm.numTimes;
  num get height  => _height;
  num get tabMargin  => _tabMargin;
  num get minWidth  => _minWidth;
  num get leftMarginCheckbox  => _leftMarginCheckbox;

  // renders tabs
  renderTabs(){
    // background frame
    String prevFillStyle = _context.strokeStyle;
    _context
      ..fillStyle = Theme.tsFillMainContent
      ..strokeStyle = Theme.tsFillMainContent;

    _context
      ..beginPath()
      ..rect(0, _topMargin, _width, _height)
      ..closePath()
      ..fill()
      ..stroke();

    num tabMaxWidth;
    num seqMinTabs = 0;
    bool isGroupHighlighted = false;
      tm.forEach((var s){
        if (tm.isMinimized(s)) {
          seqMinTabs++;
          if (tm.isHighlighted(s)) {
            isGroupHighlighted = true;
          }
          if(tm.getIndex(s) == (tm.numTimes-1)){
            num firstMin = tm.getIndex(s) - seqMinTabs;
            num pos = tm.getPosX1(tm.elementAt(firstMin+1)) + _tabMargin;
            if(isGroupHighlighted && seqMinTabs > 1){
              renderMinHighlighted(pos, tabMaxWidth);
              if(tabMaxWidth > (_height - leftMarginCheckbox)) {
                renderPlus(pos, "#FFFFFF");
              }
            }
            if((tabMaxWidth > (_height - leftMarginCheckbox)) && seqMinTabs > 1) {
              renderNumTabs(pos, seqMinTabs.toString());
            }
          }
        }else{
          if(seqMinTabs>=2) {
            num firstMin = tm.getIndex(s) - seqMinTabs;
            num pos = tm.getPosX1(tm.elementAt(firstMin)) + _tabMargin;
            if(isGroupHighlighted){
              renderMinHighlighted(pos, tabMaxWidth);
              if(tabMaxWidth > (_height - leftMarginCheckbox)) {
                renderPlus(pos, "#FFFFFF");
              }
            }
            if (tabMaxWidth > (_height - leftMarginCheckbox)) {
              renderNumTabs(pos, seqMinTabs.toString());
            }
          }
          seqMinTabs = 0;
          isGroupHighlighted = false;
        }

        if(seqMinTabs==0 || seqMinTabs==1) {
          String fontStyle = "lighter " + (_fontSize - 2).toString() + "px " +
              CANVAS_FONT;
          if (tm.isVisible(s)) {
            tabMaxWidth = tm.getWidth(s);
            num pos = tm.getPosX1(s) + _tabMargin;
            if (tm.isSelected(s)) {
              fontStyle =
                  "bold " + (_fontSize).toString() + "px " + CANVAS_FONT;
            } else if (tm.isHighlighted(s)) {
              fontStyle = (_fontSize).toString() + "px " + CANVAS_FONT;
            }

            num radius = 13;
            num minHeight = 0;
            if (tm.isMinimized(s)) {
              minHeight =
                  (_graph.nodes.getHeightAll()) + _statsHeight + _topMargin +
                      _height;
            }
            //render tab with a rectangle with rounded top corners
            _context
              ..beginPath()
              ..moveTo(pos + radius, 0)
              ..lineTo(pos + tabMaxWidth - radius, 0)
              ..quadraticCurveTo(
                  pos + tabMaxWidth, 0, pos + tabMaxWidth, 0 + radius)
              ..lineTo(pos + tabMaxWidth, minHeight + _height)..lineTo(
                pos, minHeight + _height)..lineTo(pos, 0 + radius)
              ..quadraticCurveTo(pos, 0, pos + radius, 0)
              ..fillStyle = "#EAEAEA"
              ..strokeStyle = "#D7D7D7"
              ..closePath()
              ..fill()
              ..stroke();

            TabCheckbox tc = new TabCheckbox(
                _canvas, pos, 0, leftMarginCheckbox);
            TabMinimize tmin = new TabMinimize(
                _canvas, pos, 0, tabMaxWidth - leftMarginCheckbox);
            if (tm.isSelected(s)) {
              //render a colored semi transparent rectangle for selected TS
              renderSelected(pos, tabMaxWidth);
              if (!tm.isMinimized(s)) {
                //hide checkbox and minimize icon if there isn't space for render it
                if (tabMaxWidth > (leftMarginCheckbox + (_height * 1.5))) {
                  tc.render(true);
                  tmin.render();
                }
              } else {
                if (tabMaxWidth > (_height - leftMarginCheckbox)) {
                  tc.render(true);
                  renderPlus(pos, "#999999");
                }
              }
            } else {
              if (!tm.isMinimized(s)) {
                //hide checkbox and minimize icon if there isn't space for render it
                if (tabMaxWidth > (leftMarginCheckbox + (_height * 1.5))) {
                  tc.render();
                  tmin.render();
                }
              } else {
                if (tabMaxWidth > (_height - leftMarginCheckbox)) {
                  tc.render();
                  renderPlus(pos, "#999999");
                }
                if (tm.isHighlighted(s)) {
                  renderMinHighlighted(pos, tabMaxWidth);
                  if(tabMaxWidth > (_height - leftMarginCheckbox)) {
                    renderPlus(pos, "#FFFFFF");
                  }
                }
              }
            }
          }
        }
      });
    _context
      ..fillStyle = prevFillStyle;
  }

  void renderPlus(num pos, String color) {
    _context
      ..strokeStyle = color
      ..beginPath()
      ..strokeText("+", pos + leftMarginCheckbox,
          ((_graph.nodes.getHeightAll() + _height) / 2) +
              _topMargin + _statsHeight)
      ..closePath()
      ..fill()
      ..stroke();
  }

  void renderSelected(num pos, num tabMaxWidth) {
    _context
      ..fillStyle = 'rgba(102,153,225,0.3)'
      ..beginPath()
      ..rect(pos, _topMargin + _height, tabMaxWidth,
          (_graph.nodes.getHeightAll()) + _statsHeight + _topMargin +
              _height)
      ..closePath()
      ..fill()
      ..stroke();
  }

  renderMinHighlighted(num pos, num tabMaxWidth) {
    _context
      ..fillStyle = "#1e7e34"
      ..beginPath()
      ..rect(pos, _topMargin + _height, tabMaxWidth,
          (_graph.nodes.getHeightAll()) + _statsHeight +
              _topMargin + _height)
      ..closePath()
      ..fill()
      ..stroke();
  }

  renderNumTabs(num pos, String seqMinTabs){
    _context
      ..strokeText("["+ seqMinTabs +"]", pos + leftMarginCheckbox,
          ((_graph.nodes.getHeightAll() + _height) / 2) +
              _topMargin + _statsHeight - _height);
  }
}
