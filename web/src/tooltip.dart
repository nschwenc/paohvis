part of paohvis;

class Tooltip{
  CanvasElement _canvas;
  CanvasRenderingContext2D _context;
  String _message;
  Point _position;
  bool _visibility;
  bool _alignRight = true;
  DivElement _ttip = new DivElement();

  // reference to canvas and to the HTML tooltip
  Tooltip(CanvasElement canvasElement, [DivElement ttip]){
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    if (ttip != null){
      _ttip = ttip.childNodes[1]; //inner div
    }
  }

  void setup(){
    setTooltip(new Point(0,0), "");
  }

  void setTooltip (Point p, String msg){
    _position = p;
    _message = msg;
  }
  void setTooltipAlignRight (bool alignRight){
    _alignRight = alignRight;
  }

  void setTooltipPos(Point p){
    _position = p;
  }

  void setTooltipMessage (String msg){
    _message = msg;
  }

  void setTooltipVisible(bool vis){
    _visibility = vis;
    _ttip.style.visibility = _visibility? 'visible' : 'hidden';
  }
  // TODO: rendering can be more complex than here, with more information
  void render() {
//    _ttip.style.visibility = _visibility? 'visible' : 'hidden';
    if (ENABLED_TOOLTIP) {
      /*
      // render in canvas...
      String prevFillStyle = "@c0ffee";
      String prevFont = "12px Helvetica";
      int textHeight = 12;
      int textMargin = 2;
      int maxwWidth = 400;
      prevFillStyle = _context.fillStyle;
      prevFont = _context.font;

      _context
        ..fillStyle = "yellow"
        ..strokeStyle = "black"
        ..font = textHeight.toString() + "px Helvetica"
        ..fillRect(_position.x, _position.y - textHeight - textMargin, _context.measureText(_message).width + textMargin*2, textHeight + textMargin)
        ..stroke()
        ..rect(_position.x,  _position.y - textHeight - textMargin, _context.measureText(_message).width + textMargin*2, textHeight + textMargin)
        ..stroke()
        ..fillStyle = "red"
        ..fillText(_message,
            _position.x + textMargin,
            _position.y - textMargin,
            maxwWidth)
        ..stroke()
        ..fillStyle = prevFillStyle
        ..font = prevFont;
    */
    // html tooltip
      _ttip.innerHtml = _message;
      _ttip.style.top = _position.y.toString() + "px";

      bool alignRight= _alignRight;
      alignRight = alignRight && (_position.x - _ttip.clientWidth >= 0) ;

      if(alignRight){
        _ttip.style.left = (_position.x-_ttip.clientWidth).toString() + "px";
        _ttip.style.textAlign = "right";
      }
      else{
        _ttip.style.left = _position.x.toString() + "px";
        _ttip.style.textAlign = "left";
      }

    }
  }
}