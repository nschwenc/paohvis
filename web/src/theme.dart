library themes;

import '../globals.dart';
/**
 * Class with static methods to manage application theme
 */
class Theme{
  static String _edgesDefault = "#777777";
  static String _edgesNotHighlight = "#dddddd";
  static String _verticesDefault = "#333333";
  static String _verticesNotHighlight = "#bbbbbb";

  //PAOVIS - Color of vertices and edges
  static String _verticesPaovisDefault = "#333333";
  static String _verticesPaovisHighlight = "#4caf50";
  static String _verticesPaovisNotHighlight = "#cccccc";
  static String _verticesPaovisSelect = "#d60000";
  static String _adjacentVerticesPaovisSelect = "#31a354";
  static String _adjacentVerticesPaovisHighlight = "#4caf50";

  static String _edgesPaovis = "#777777";
  static String _edgesPaovisDefault = "#0033AA";
  static String _edgesPaovisHighlight = "#4caf50";
  static String _edgesPaovisNotHighlight = "#dddddd";
  static String _edgesPaovisIntersect = "#111111";
  static String _edgesPaovisSelect = "#888888";

  //CURVES - Color of edges
  static String _edgesCurvesDefault = "#555555";
  static String _edgesCurvesNotHighlight = "#cccccc";
  static String _edgesCurvesHighlight = "#4caf50";
  static String _edgesCurvesSelect = "#CE5D5D";

  //SPLAT - Color of Vertices and Edges
  static String _verticesSplatDefault = "#555555";
  static String _verticesSplatSelect = "#d60000";
  static String _verticesSplatHighlight = "#4caf50";
  static String _verticesSplatNotHighlight = "#cccccc";

  static String _edgesSplatDefault = "#555555";
  static String _edgesSplatCommunity = "#003DF3";
  static String _edgesSplatNotHighlight = "#cccccc";
  static String _edgesSplatHighlight = "#4caf50";
  static String _edgesSplatSelect = "#CE5D5D";
  static String _adjacentVerticesSplatSelect = "#31a354";
  static String _adjacentVerticesSplatHighlight = "#4caf50";

  //NODE-LINK - Color of Vertices and Edges
  static String _verticesNodelinkDefault = "#333333";
  static String _verticesNodelinkNotHighlight = "#cccccc";
  static String _verticesNodelinkSelect = "#d60000";
  static String _verticesNodelinkHighlight= "#4caf50";
  static String _adjacentVerticesNodelinkSelect = "#31a354";
  static String _adjacentVerticesNodelinkHighlight = "#31a354";

  static String _edgesNodelinkDefault = "#777777";
  static String _edgesNodelinkHighlight = "#4caf50";
  static String _edgesNodelinkNotHighlight = "#cccccc";
  static String _edgesNodelinkSelect = "#CE5D5D";

  //GENERAL - Color of others canvas element
  //Canvas
  static String _canvasBackground = "#ffffff"; //canvas
  static String _canvasBorderBackground = "#ffffff"; //canvas border

  //Vertices content
  static String _verticesFillMainContent = "#ffffff";
  static String _verticesBorderMainContent = "#ffffff";

  //Vertices symbol and font for each state
  static String _fillVerticesSymbol = "black";
  static String _borderVerticesSymbol = "black";
  static String _verticesSymbolFillHighlight = "#4caf50";
  static String _verticesSymbolBorderHighlight = "#4caf50";
  static String _verticesSymbolFillSelect = "#d60000";
  static String _verticesSymbolBorderSelect = "#d60000";
  static String _verticesSymbolFontDefault = "black";
  static String _verticesSymbolFontHighlight = "#4caf50";
  static String _verticesSymbolFontSelect = "#d60000";

// Adjacent vertices symbol and font for each state
  static String _adjacentVerticesSymbolFillHighlight = "#4caf50";
  static String _adjacentVerticesSymbolBorderHighlight = "#a5d6a7";
  static String _adjacentVerticesSymbolFillSelect = "#31a354";
  static String _adjacentVerticesSymbolBorderSelect = "#a5d6a7";
  static String _adjacentVerticesSymbolFontHighlight = "#4caf50";
  static String _adjacentVerticesSymbolFontSelect = "#31a354";

  //Time slot and font
  static String _tsFillMainContent = "#ffffff";
  static String _tsBorderMainContent = "ffffff";
  static String _tsFontDefault = "#333333";
  static String _tsFontHighlight = "#4caf50";
  static String _tsFontNotHighlight = "#cccccc";

  //Sparkline
  static List<String> _ALT_COLORS = ["#000000", "#ffffff"];
  static String _sparklineBackground = "#ffffff"; //if ALT_COLORS = null
  static String _sparklineHighlight = "#ccffcc";
  static String _sparklineSelect = "#d29696";

  //Linegraph
  static String _histogramBackgroundLinegraph = "#656565";
  static String _histogramBorderLinegraph = "#333333";

  //stats
  static String _statsBackground = "#ffffff";//"#dc3d2a"; //canvas
  static String _statsBorderBackground = _statsBackground; //canvas border
  static String _statsLabels = _fillVerticesSymbol;
  static String _statsLabelsBackground = "#d9dcdd";// lightgray - "#efa70d"; //orange
  static String _statsLabelsForeground = "#b0b4b5";// gray - "#fffc01"; //yellow


  static resetTheme(){ // in case something is wrong default values are set
    if(DEBUG) print("called resetTheme");
    //PAOVIS - Color of vertices and edges
    _verticesPaovisDefault = "#333333";
    _verticesPaovisHighlight = "#4caf50";
    _verticesPaovisNotHighlight = "#cccccc";
    _verticesPaovisSelect = "#d60000";
    _adjacentVerticesPaovisSelect = "#31a354";
    _adjacentVerticesPaovisHighlight = "#4caf50";

    _edgesPaovis = "#777777";
    _edgesPaovisDefault = "#0033AA";
    _edgesPaovisHighlight = "#4caf50";
    _edgesPaovisNotHighlight = "#cccccc";
    _edgesPaovisIntersect = "#111111";
    _edgesPaovisSelect = "#888888";

    //CURVES - Color of edges
    _edgesCurvesDefault = "#555555";
    _edgesCurvesHighlight = "#4caf50";
    _edgesCurvesNotHighlight = "#cccccc";
    _edgesCurvesSelect = "#CE5D5D";

    //SPLAT - Color of Vertices and Edges
    _verticesSplatDefault = "#333333";
    _verticesSplatHighlight = "#4caf50";
    _verticesSplatSelect = "#d60000";
    _adjacentVerticesSplatSelect = "#31a354";
    _adjacentVerticesSplatHighlight = "#a5d6a7";
    _verticesSplatNotHighlight = "#cccccc";

    _edgesSplatDefault = "#333333";
    _edgesSplatCommunity = "#003DF3";
    _edgesSplatHighlight = "#4caf50";
    _edgesSplatNotHighlight = "#cccccc";
    _edgesSplatSelect = "#CE5D5D";

    //NODE-LINK - Color of Vertices and Edges
    _verticesNodelinkDefault = "#333333";
    _verticesNodelinkNotHighlight = "#cccccc";
    _verticesNodelinkSelect = "#d60000";
    _verticesNodelinkHighlight= "#4caf50";
    _adjacentVerticesNodelinkSelect = "#31a354";
    _adjacentVerticesNodelinkHighlight= "#31a354";


    _edgesNodelinkDefault = "#777777";
    _edgesNodelinkHighlight = "#4caf50";
    _edgesNodelinkNotHighlight = "#cccccc";
    _edgesNodelinkSelect = "#CE5D5D";

    //GENERAL - Color of others canvas element
    //Canvas
    _canvasBackground = "#ffffff";
    _canvasBorderBackground = "#ffffff";

    //Vertices content
    _verticesFillMainContent = "#ffffff";
    _verticesBorderMainContent = "#ffffff";

    //Vertices symbol and font for each state
    _fillVerticesSymbol = "black";
    _borderVerticesSymbol = "black";

    _verticesSymbolFillHighlight = "#4caf50";
    _verticesSymbolBorderHighlight = "#4caf50";
    _verticesSymbolFillSelect = "#d60000";
    _verticesSymbolBorderSelect = "#d60000";
    _verticesSymbolFontDefault = "#333333";
    _verticesSymbolFontHighlight = "#4caf50";
    _verticesSymbolFontSelect = "#d29696";

    //Adjacent vertices symbol and font for each state
    _adjacentVerticesSymbolFillHighlight = "#a5d6a7";
    _adjacentVerticesSymbolBorderHighlight = "#a5d6a7";
    _adjacentVerticesSymbolFillSelect = "#31a354";
    _adjacentVerticesSymbolBorderSelect = "#a5d6a7";
    _adjacentVerticesSymbolFontHighlight = "#4caf50";
    _adjacentVerticesSymbolFontSelect = "#31a354";

    //Time slot and font
    _tsFillMainContent = "#ffffff";
    _tsBorderMainContent = "white";

    _tsFontDefault = "#333333";
    _tsFontHighlight = "#4caf50";
    _tsFontNotHighlight = "#cccccc";

    //Sparkline
    _ALT_COLORS = ["#000000", "#ffffff"];
    _sparklineBackground = "#cccccc"; //if ALT_COLORS = null
    _sparklineHighlight = "#ccffcc";
    _sparklineSelect = "#d29696";

    //Linegraph
    _histogramBackgroundLinegraph = "#656565";
    _histogramBorderLinegraph = "#333333";
  }

  // getter and setter for each private color variable

  static get edgesDefault => _edgesDefault;
  static void setEdgesDefault(String newCol){_edgesDefault = newCol;}

  static get edgesNotHighlight => _edgesNotHighlight;
  static void setEdgesNotHighlight(String newCol){_edgesNotHighlight = newCol;}

  static get verticesDefault=> _verticesDefault;
  static void setVerticesDefault(String newCol){_verticesDefault = newCol;}

  static get verticesNotHighlight=> _verticesNotHighlight;
  static void setVerticesNotHighlight(String newCol){_verticesNotHighlight = newCol;}



  static get edgesCurvesDefault => _edgesCurvesDefault;
  static void setEdgesCurvesDefault(String newCol){_edgesCurvesDefault = newCol;}

  static get edgesCurvesHighlight => _edgesCurvesHighlight;
  static void setEdgesCurvesHighlight(String newCol){_edgesCurvesHighlight = newCol;}

  static get edgesCurvesNotHighlight => _edgesCurvesNotHighlight;
  static void setEdgesCurvesNotHighlight(String newCol){_edgesCurvesNotHighlight = newCol;}

  static get edgesCurvesSelect => _edgesCurvesSelect;
  static void setEdgesCurvesSelect(String newCol){_edgesCurvesSelect = newCol;}

  static get edgesSplatDefault => _edgesSplatDefault;
  static void setEdgesSplatDefault(String newCol){_edgesSplatDefault = newCol;}

  static get edgesSplatCommunity => _edgesSplatCommunity;
  static void setEdgesSplatCommunity(String newCol){_edgesSplatCommunity = newCol;}

  static get edgesSplatHighlight => _edgesSplatHighlight;
  static void setEdgesSplatHighlight(String newCol){_edgesSplatHighlight = newCol;}

  static get edgesSplatNotHighlight => _edgesSplatNotHighlight;
  static void setEdgesSplatNotHighlight(String newCol){_edgesSplatNotHighlight = newCol;}

  static get edgesSplatSelect => _edgesSplatSelect;
  static void setEdgesSplatSelect(String newCol){_edgesSplatSelect = newCol;}


  static get verticesNodelinkDefault=> _verticesNodelinkDefault;
  static void setVerticesNodelinkDefault(String newCol){_verticesNodelinkDefault = newCol;}

  static get verticesNodelinkNotHighlight=> _verticesNodelinkNotHighlight;
  static void setVerticesNodelinkNotHighlight(String newCol){_verticesNodelinkNotHighlight = newCol;}

  static get verticesNodelinkSelect=> _verticesNodelinkSelect;
  static void setVerticesNodelinkSelect(String newCol){_verticesNodelinkSelect = newCol;}

  static get verticesNodelinkHighlight=> _verticesNodelinkHighlight;
  static void setVerticesNodelinkHighlight(String newCol){_verticesNodelinkHighlight = newCol;}

  static get adjacentVerticesNodelinkSelect=> _adjacentVerticesNodelinkSelect;
  static void setAdjacentVerticesNodelinkSelect(String newCol){_adjacentVerticesNodelinkSelect = newCol;}

  static get adjacentVerticesNodelinkHighlight=> _adjacentVerticesNodelinkHighlight;
  static void setAdjacentVerticesNodelinkHighlight(String newCol){_adjacentVerticesNodelinkHighlight = newCol;}


  static get edgesNodelinkDefault => _edgesNodelinkDefault;
  static void setEdgesNodelinkDefault(String newCol){_edgesNodelinkDefault = newCol;}

  static get edgesNodelinkHighlight => _edgesNodelinkHighlight;
  static void setEdgesNodelinkHighlight(String newCol){_edgesNodelinkHighlight = newCol;}

  static get edgesNodelinkNotHighlight => _edgesNodelinkNotHighlight;
  static void setEdgesNodelinkNotHighlight(String newCol){_edgesNodelinkNotHighlight = newCol;}

  static get edgesNodelinkSelect => _edgesNodelinkSelect;
  static void setEdgesNodelinkSelect(String newCol){_edgesNodelinkSelect = newCol;}



  static get edgesPaovisDefault => _edgesPaovisDefault;
  static void setEdgesPaovisDefault(String newCol){_edgesPaovisDefault = newCol;}

  static get edgesPaovis => _edgesPaovis;
  static void setEdgesPaovis(String newCol){_edgesPaovis = newCol;}

  static get edgesPaovisHighlight=> _edgesPaovisHighlight;
  static void setEdgesPaovisHighlight(String newCol){_edgesPaovisHighlight = newCol;}

  static get edgesPaovisSelect=> _edgesPaovisSelect;
  static void setEdgesPaovisSelect(String newCol){_edgesPaovisSelect = newCol;}

  static get edgesPaovisNotHighlight=> _edgesPaovisNotHighlight;
  static void setEdgesPaovisNotHighlight(String newCol){_edgesPaovisNotHighlight = newCol;}

  static get edgesPaovisIntersect=> _edgesPaovisIntersect;
  static void setEdgesPaovisIntersect(String newCol){_edgesPaovisIntersect = newCol;}

  static get verticesSplatDefault=> _verticesSplatDefault;
  static void setVerticesSplatDefault(String newCol){_verticesSplatDefault = newCol;}

  static get verticesPaovisDefault=> _verticesPaovisDefault;
  static void setVerticesPaovisDefault(String newCol){_verticesPaovisDefault = newCol;}

  static get verticesPaovisHighlight=> _verticesPaovisHighlight;
  static void setVerticesPaovisHighlight(String newCol){_verticesPaovisHighlight = newCol;}

  static get verticeSplatHighlight=> _verticesSplatHighlight;
  static void setVerticesSplatHighlight(String newCol){_verticesSplatHighlight = newCol;}

  static get verticesPaovisNotHighlight=> _verticesPaovisNotHighlight;
  static void setVerticesPaovisNotHighlight(String newCol){_verticesPaovisNotHighlight = newCol;}

  static get verticesPaovisSelect=> _verticesPaovisSelect;
  static void setVerticesPaovisSelect(String newCol){_verticesPaovisSelect = newCol;}

  static get verticesSplatSelect=> _verticesSplatSelect;
  static void setVerticesSplatSelect(String newCol){_verticesSplatSelect = newCol;}

  static get verticesSplatHighlight=> _verticesSplatHighlight;
  static void setVerticesSplatHihlight(String newCol){_verticesSplatHighlight = newCol;}

  static get verticesSplatNotHighlight=> _verticesSplatNotHighlight;
  static void setVerticesSplatNotHighlight(String newCol){_verticesSplatNotHighlight = newCol;}

  static get adjacentVerticesPaovisSelect=> _adjacentVerticesPaovisSelect;
  static void setAdjacentVerticesPaovisSelect(String newCol){_adjacentVerticesPaovisSelect = newCol;}

  static get adjacentVerticesSplatSelect=> _adjacentVerticesSplatSelect;
  static void setAdjacentVerticesSplatSelect(String newCol){_adjacentVerticesSplatSelect = newCol;}

  static get adjacentVerticesPaovisHighlight=> _adjacentVerticesPaovisHighlight;
  static void setAdjacentVerticesPaovisHighlight(String newCol){_adjacentVerticesPaovisHighlight = newCol;}

  static get adjacentVerticesSplatHighlight=> _adjacentVerticesSplatHighlight;
  static void setAdjacentVerticesSplatHighlight(String newCol){_adjacentVerticesSplatHighlight = newCol;}



  static get canvasBackground => _canvasBackground;
  static void setCanvasBackground(String coloreBGint){_canvasBackground = coloreBGint;}

  static get statsBackground => _statsBackground;
  static void setStatsBackground(String coloreBGint){_statsBackground = coloreBGint;}

  static get statsLabels => _statsLabels;
  static void setStatsLabels(String coloreBGint){_statsLabels = coloreBGint;}

  static get statsLabelsBackground => _statsLabelsBackground;
  static void setStatsLabelsBackground(String coloreBGint){_statsLabelsBackground = coloreBGint;}

  static get statsLabelsForeground => _statsLabelsForeground;
  static void setStatsLabelsForeground(String coloreBGint){_statsLabelsForeground = coloreBGint;}

  static get canvasBorderBackground => _canvasBorderBackground;
  static void setCanvasBorderBackground(String coloreBGint){_canvasBorderBackground = coloreBGint;}



  static get verticesSymbolFontDefault => _verticesSymbolFontDefault;
  static void setVerticesSymbolFontDefault(String newCol){_verticesSymbolFontDefault = newCol;}

  static get adjacentVerticesSymbolFontSelect => _adjacentVerticesSymbolFontSelect;
  static void setAdjacentVerticesSymbolFontSelect(String newCol){_adjacentVerticesSymbolFontSelect = newCol;}

  static get adjacentVerticesSymbolFontHighlight => _adjacentVerticesSymbolFontHighlight;
  static void setAdjacentVerticesSymbolFontHighlight(String newCol){_adjacentVerticesSymbolFontHighlight = newCol;}

  static get verticesSymbolFontHighlight => _verticesSymbolFontHighlight;
  static void setVerticesSymbolFontHighlight(String newCol){_verticesSymbolFontHighlight = newCol;}

  static get verticesSymbolFontSelect => _verticesSymbolFontSelect;
  static void setVerticesSymbolFontSelect(String newCol){_verticesSymbolFontSelect = newCol;}



  static get tsFillMainContent => _tsFillMainContent;
  static void setTsFillMainContent(String newCol){_tsFillMainContent = newCol;}

  static get tsBorderMainContent => _tsBorderMainContent;
  static void setTsBorderMainContent(String newCol){_tsBorderMainContent = newCol;}

  static get tsFontDefault => _tsFontDefault;
  static void setTsFontDefault(String newCol){_tsFontDefault = newCol;}

  static get tsFontHighlight => _tsFontHighlight;
  static void setTsFontHighlight(String newCol){_tsFontHighlight = newCol;}

  static get tsFontNotHighlight => _tsFontNotHighlight;
  static void setTsFontNotHighlight(String newCol){_tsFontNotHighlight = newCol;}



  static get sparklineHighlight => _sparklineHighlight;
  static void setSparklineHighlight(String newCol){_sparklineHighlight = newCol;}

  static get sparklineSelect => _sparklineSelect;
  static void setSparklineSelect(String newCol){_sparklineSelect = newCol;}

  static get sparklineBackground => _sparklineBackground;
  static void setSparklineBackground(String newCol){_sparklineBackground = newCol;}



  static get histogramBackgroundLinegraph => _histogramBackgroundLinegraph;
  static void setHistogramBackgroundLinegraph(String newCol){_histogramBackgroundLinegraph = newCol;}

  static get histogramBorderLinegraph => _histogramBorderLinegraph;
  static void setHistogramBorderLinegraph(String newCol){_histogramBorderLinegraph = newCol;}



  static get verticesFillMainContent => _verticesFillMainContent;
  static void setVerticesFillMainContent(String newCol){_verticesFillMainContent = newCol;}

  static get verticesBorderMainContent => _verticesBorderMainContent;
  static void setVerticesBorderMainContent(String newCol){_verticesBorderMainContent = newCol;}

  static get fillVerticesSymbol => _fillVerticesSymbol;
  static void setFillVerticesSymbol(String newCol){_fillVerticesSymbol = newCol;}

  static get borderVerticesSymbol => _borderVerticesSymbol;
  static void setBorderVerticesSymbol(String newCol){_borderVerticesSymbol = newCol;}

  static get adjacentVerticesSymbolFillSelect => _adjacentVerticesSymbolFillSelect;
  static void setAdjacentVerticesSymbolFillSelect(String newCol){_adjacentVerticesSymbolFillSelect = newCol;}

  static get adjacentVerticesSymbolBorderSelect => _adjacentVerticesSymbolBorderSelect;
  static void setAdjacentVerticesSymbolBorderSelect(String newCol){_adjacentVerticesSymbolBorderSelect = newCol;}

  static get adjacentVerticesSymbolFillHighlight => _adjacentVerticesSymbolFillHighlight;
  static void setAdjacentVerticesSymbolFillHighlight(String newCol){_adjacentVerticesSymbolFillHighlight = newCol;}

  static get adjacentVerticesSymbolBorderHighlight => _adjacentVerticesSymbolBorderHighlight;
  static void setAdjacentVerticesSymbolBorderHighlight(String newCol){_adjacentVerticesSymbolBorderHighlight = newCol;}

  static get verticesSymbolFillHighlight => _verticesSymbolFillHighlight;
  static void setVerticesSymbolFillHighlight(String newCol){_verticesSymbolFillHighlight = newCol;}

  static get verticesSymbolBorderHighlight => _verticesSymbolBorderHighlight;
  static void setVerticesSymbolBorderHighlight(String newCol){_verticesSymbolBorderHighlight = newCol;}

  static get verticesSymbolFillSelect => _verticesSymbolFillSelect;
  static void setVerticesSymbolFillSelect(String newCol){_verticesSymbolFillSelect = newCol;}

  static get verticesSymbolBorderSelect => _verticesSymbolBorderSelect;
  static void setVerticesSymbolBorderSelect(String newCol){_verticesSymbolBorderSelect = newCol;}

  static get ALT_COLORS => _ALT_COLORS;
  static void setALT_COLORS(String newALT_COLORS1,String newALT_COLORS2){_ALT_COLORS[0] = newALT_COLORS1; _ALT_COLORS[1] = newALT_COLORS2;}
}