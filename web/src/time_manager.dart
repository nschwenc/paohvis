part of paohvis;

class TimeSlot{
  String _name;
  int _index;

  num _posX1;
  num _width;
//  num _computedWidth;
//  num _computedPosX1;

//  num _translateX;
//  num _scale;

  set posX1(num value) {
    _posX1 = value;
//    _computedPosX1 = _posX1;
  }

//  set translateX(num value){
//    _translateX = value;
//  }

  set width(num value) {
    _width = value;
//    _computedWidth = _width;
  }

  String get name => _name;
  int get index => _index;
  set index(int value) => _index = value;
  num get posX1 => _posX1; // * _scale + _translateX;
//  num get posX2 => posX1 + width;
  num get width => _width; // * _scale;

//  num get translateX => _translateX;

  bool isVisible = true;
  bool isMinimized = false;
  bool _isHighlighted = false;
  bool _isSelected = false;
  bool _isSideSelected = false;

  bool get isSideSelected => _isSideSelected;
  void sideSelect(){_isSideSelected = true;}
  void unsideselect(){_isSideSelected = false;}

  bool get isSelected => _isSelected;
  void select(){_isSelected = true;}
  void unselect(){_isSelected = false;}

  bool get isHighlighted => _isHighlighted;
  void highlight(){_isHighlighted = true;}
  void unhighlight(){_isHighlighted = false;}

//  void scaleWidth(scale){
//    _scale = scale;
////    _computedPosX1 = _posX1*scale;
////    _computedWidth = _width*scale;
//  }

  TimeSlot(String name, int index){
    _name = name;
    _index = index;
//    _scale = 1;
//    _translateX = 0;
    _width = 0;
    _posX1 = 0;
  }

}

class TimeManager extends IterableBase { //TODO: create  new class TimeSlotManager
  static final TimeManager _singleton = new TimeManager._internal();

  factory TimeManager() =>  _singleton;

  List<String> timeList = new List<String> (); // this is for time ordering TODO: manage better time order
  Map<String, TimeSlot>   timeIndex = new Map<String, TimeSlot>();

  bool _anyTimeSlotHighlighted = false;
  bool get anyTimeSlotHighlighted => _anyTimeSlotHighlighted;
  set anyTimeSlotHighlighted(bool b) => _anyTimeSlotHighlighted = b;

  bool _anyTimeSlotSelected = false;
  bool get anyTimeSlotSelected => _anyTimeSlotSelected;
  set anyTimeSlotSelected(bool b) => _anyTimeSlotSelected = b;

  num _margin = 0;

  num get margin => _margin;

  set margin(num value) {
    _margin = value;
  } // left margin (nodes framewidth)

  num _width = 0;
  num _sep = TIME_SLOT_SEP; // separation between time slots

//  num _shiftX = 0;
  num _shiftY = 0;

  num _scale = 1;
  num _translateX = 0;

  /// /*Constructor*/
  TimeManager._internal();

  void sortTimes() {
    int index = 0;
    if (isNumeric(timeList[0])) {
      timeList.sort((a,b) => int.parse(a).compareTo(int.parse(b)));
    }
    else{
      timeList.sort();
    }
    timeList.forEach((ts){
      timeIndex[ts].index = index;
      index++;
    });
  }


  void reset(){
    _shiftY = 0;
    _margin = 0;
    _width = 0;
    timeList.clear();
    timeIndex.clear();
  }

  get iterator {
    return timeList.iterator;
  }

  get numberOfTimeSlots{ return timeList.length; }


  setup(){
//    if(!(BIOFABRIC || CURVES || SPLAT || NODELINK)) {
//      setWidthAll(MIN_TS_WIDTH);
//    }
//    computePosXAll();
    setTranslateAll(margin);

//    _shiftX = 0;
    _shiftY = 0;
  }

  num getTranslateTs(its){
    return getPosX1(timeList[its]);
  }

  int get numTimes => timeList.length;

  bool isMinimized(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].isMinimized;
    return true;
  }

  void setMinimized(String ts, bool minimized){
    timeIndex[ts].isMinimized = minimized;
  }

  bool isPrevMinimized(String ts){
    if(timeIndex.containsKey(elementAt(getIndex(ts)-1)))
      return timeIndex[elementAt(getIndex(ts)-1)].isMinimized;
    return true;
  }

  bool isFirst(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].index == 0;
    return false;
  }

  bool isLast(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].index == numTimes;
    return false;
  }

  bool isVisible(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].isVisible;
    return false;
  }

  void setVisible(String ts, bool visible){
    timeIndex[ts].isVisible = visible;
  }

  int getIndex(String ts){
    return timeIndex[ts].index;
  }

  TimeSlot getTimeSlot(String ts){
    return timeIndex[ts];
  }

  //returns posX1 checking also if the TS is minimized
  num getPosX1Min(String ts){
    if(timeIndex.containsKey(ts) && !timeIndex[ts].isMinimized)
      return timeIndex[ts].posX1 * _scale + _translateX;
    return 0; // raise exception
  }

  num getPosX1(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].posX1 * _scale + _translateX;
    return 0; // raise exception
  }
  num getPosX2(String ts){
    if(timeIndex.containsKey(ts))
      return getPosX1(ts) + getWidth(ts);
    return 0; // raise exception
  }
  num getShift(num i){
    if(i < timeList.length && timeIndex.containsKey(timeList[i]))
      return getPosX1(timeList[i]);//timeIndex[timeList[i]].posX1 + timeIndex[timeList[i]].width*_scale;
    return 0; // raise exception
  }
  bool isHighlighted(String ts){
    return timeIndex[ts].isHighlighted;
  }
  // highlights the label where the mouse moves
  void highlight(String ts){
    timeIndex[ts].highlight();
    _anyTimeSlotHighlighted = true;
  }
  void unhighlight(String ts){
    timeIndex[ts].unhighlight();
  }

  bool isSelected(String ts){
    return timeIndex[ts].isSelected;
  }
  void select(String ts){
    timeIndex[ts].select();
    _anyTimeSlotHighlighted = true;
  }
  void unselect(String ts){
    timeIndex[ts].unselect();
  }

  bool isSideSelected(String ts){
    return timeIndex[ts].isSideSelected;
  }
  void sideSelect(String ts){
    timeIndex[ts].sideSelect();
//    _anyTimeSlotHighlighted = true;
  }
  void unsideselect(String ts){
    timeIndex[ts].unsideselect();
  }

  num getWidth(String ts){
    if(timeIndex.containsKey(ts))
      return timeIndex[ts].width * _scale - _sep;
    return 0; // raise exception
  }
  void setWidth(String ts, num width){
    timeIndex[ts].width = width;
  }
  void setWidthAll(num width){
    timeIndex.forEach((ts, value) => value.width = width);
  }
  num getWidthAll(){
    num width = 0;
    timeList.forEach((ts) => width += getWidth(ts) + _sep);
    return width;
  }

  num getWidthAllN(){
    num width = 0;
    timeList.forEach((ts) => width += timeIndex[ts].width);
    return width;
  }

  void scaleWidth(num scale){
    _scale = scale;
//    timeIndex.forEach((ts, value) => value.scaleWidth(scale));
  }
  void computePosXAll(){
    num cumWidth = 0;
    timeList.forEach((ts){
      timeIndex[ts].posX1 = cumWidth;
//      timeIndex[ts].translateX = shift;
      cumWidth += timeIndex[ts].width;
    });
  }
  void translateAll(num tr){

    _translateX += tr;
    _translateX = min(_translateX, margin);
//    timeIndex.forEach((ts, value){
//      value.translateX += tr;
//    });
  }

  void setTranslateAll(num tr){
    _translateX = min(tr, margin);
//    timeIndex.forEach((ts, value){
//      value.translateX = tr;
//    });
  }

  void addTime(String ts){
    if(ts==null) return;
    int index = timeList.length;
    TimeSlot timeSlot = new TimeSlot(ts, index);
    if(!timeIndex.containsKey(ts)) {
      timeIndex[ts] = timeSlot;
      timeList.add(ts);
    }
  }


  int firstVisibleIdx(){
    bool found = false;
    for(int i = 0; i < timeList.length; i++){
      found = isVisible(timeList[i]);
      if(found) {
        return i;
      }
    }
    return -1;
  }

  int lastVisibleIdx(){
    bool found = false;
    for(int i = timeList.length-1; i >= 0 ; i--){
      found = isVisible(timeList[i]);
      if(found) {
        return i;
      }
    }
    return -1;
  }

  String firstVisible(){
    int idx = firstVisibleIdx();
    if(idx != -1) return timeList[idx];
    return '';
  }

  String lastVisible() {
    int idx = lastVisibleIdx();
    if(idx != -1) return timeList[idx];
    return '';
  }

  num get width => _width;

  set width(num value) {
    _width = value;
  }

}