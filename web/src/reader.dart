part of paohvis;

class Reader {
  Graph mainGraph;
  TimeManager tm;
  final String JSONSTRING = "json";
  final String ENTRYTAGS = "entryTags";
  final String ENTRYTYPE = "entryType";
  final String AUTHOR = "author";
  final String YEAR = "year";
  final String TITLE = "title";
  final String BOOKTITLE="booktitle";
  final String META="meta";
  final String LIST="list";
  final String DESCRIPTION="name";
  final String BIBURL="biburl";

  Reader(Graph g, TimeManager ntm){
      mainGraph = g;
      tm = ntm;
  }

  /**
   * Parse data from bib File and sets Graph class with values
   * is a private function
   */
  void _getDataFromBibFile(String data) {
    if (DEBUG) print("I am in _getDataFromBibFile");
    List arrlist;
    var obj;
    try {
      List<String> list = new List(1);
      list[0] = data;
      var parse = new JsObject.jsify(list);
      obj = context.callMethod("fetch_bibfile", [parse]);
      arrlist = obj[JSONSTRING];
    } catch (e) {
      print(e.toString());
    }
    mainGraph.reset();
    tm.reset();

    Map<String, Node> nodeMap = new Map();
    Map<String, Map<int, Map>> edgeMap = new Map();
    List<String> l; //lista per mettere i nomi autori splittati
    int nodeid = 0; //id nodo
    int edgeId = 0; //idedge
    Map meta = null;
    // meta["meta"]["name"]="non disponibile";

    arrlist.forEach((arrayelement) {
      if (arrayelement[ENTRYTYPE] == "article" ||
          arrayelement[ENTRYTYPE] == "inproceedings") {
        if (arrayelement[ENTRYTAGS][AUTHOR] != null) {
          //setto l'anno
          if(arrayelement[ENTRYTAGS][YEAR] != null) {
            String ts = arrayelement[ENTRYTAGS][YEAR].toString().trim();

            l = arrayelement[ENTRYTAGS][AUTHOR].split(" and");
            l.forEach((i) {
              bool shouldAddNode = !nodeMap.containsKey(i.toString().trim());
              if (shouldAddNode) {
                Node newnode = new Node(nodeid);
                newnode.name = i.toString().trim();
                newnode.slm = new SparkLineManager(canvas);
                mainGraph.nodes.add(newnode);
                nodeMap[i.toString().trim()] = newnode;
                nodeid++;
              }


              //prendo la descrizione
              String descriptionedge = arrayelement[ENTRYTAGS][TITLE]
                  .toString()
                  .trim();

              //prendo booktitle
              String booktitle = arrayelement[ENTRYTAGS][BOOKTITLE]
                  .toString()
                  .trim();

              //prendo netrytype
              String entrytype = arrayelement[ENTRYTYPE].toString().trim();

              //prendo il biburl
              String biburl=arrayelement[ENTRYTAGS][BIBURL].toString().trim();

              tm.addTime(ts);

              Sparkline sl = new Sparkline([-1.0]);
              sl.timeFrame = ts;
              sl.minValue = 0.0;
              sl.maxValue = 2.0;
              nodeMap[i.toString().trim()].slm.addSparkline(sl);

              //ad ogni chiave ts ho una map. questa map ha chiave int(-> "edgeid") a cui viene associata una map per associare la descrizione "meta" e la lista dei nodi.
              if (!edgeMap.containsKey(ts)) {
                //edgeMap[ts] = new Map<int, List<Node>>();

                edgeMap[ts] = new Map<int, Map>();
              }
              if (!edgeMap[ts].containsKey(edgeId)) {
                edgeMap[ts][edgeId] = new Map();
              }
              if (!edgeMap[ts][edgeId].containsKey(LIST)) {
                edgeMap[ts][edgeId][LIST] = new List<Node>();
              }
              if (!edgeMap[ts][edgeId].containsKey(META)) {
                edgeMap[ts][edgeId][META] = new Map();
              }
              //["list"] = new List<Node>();
              //edgeMap[ts][edgeId]["meta"]= new Map();

              edgeMap[ts][edgeId][LIST].add(nodeMap[i.toString().trim()]);
              edgeMap[ts][edgeId][META][DESCRIPTION] = descriptionedge;
              edgeMap[ts][edgeId][META][AUTHOR] = arrayelement[ENTRYTAGS][AUTHOR];
              edgeMap[ts][edgeId][META][BOOKTITLE] = booktitle;
              edgeMap[ts][edgeId][META][AUTHOR] = arrayelement[ENTRYTAGS][AUTHOR];
              edgeMap[ts][edgeId][META][ENTRYTYPE] = entrytype;
              edgeMap[ts][edgeId][META][BIBURL] = biburl;
              meta = edgeMap[ts][edgeId][META];
              // meta["meta"]["name"]=description;
              // meta["meta"]=arrayelement[ENTRYTAGS][TITLE];
            });
          }
        }
      } /*else if (arrayelement[ENTRYTYPE] == "proceedings") {
        print("è proceedings");
        if (arrayelement[ENTRYTAGS][EDITOR] != null) {
          l = arrayelement[ENTRYTAGS][EDITOR].split(" and");
          l.forEach((i) {
            bool shouldAddNode = !nodeMap.containsKey(i.toString().trim());
            if (shouldAddNode) {
              Node newnode = new Node(nodeid);
              newnode.name = i.toString().trim();
              newnode.slm = new SparkLineManager(canvas);
              mainGraph.nodes.add(newnode);
              nodeMap[i.toString().trim()] = newnode;
              nodeid++;
            }
            //setto l'anno
            String ts = arrayelement[ENTRYTAGS][YEAR].toString().trim();

            //prendo la descrizione
            String description = arrayelement[ENTRYTAGS][TITLE].toString().trim();

            tm.addTime(ts);

            Sparkline sl = new Sparkline([-1.0]);
            sl.timeFrame = ts;
            sl.minValue = 0.0;
            sl.maxValue = 2.0;
            nodeMap[i.toString().trim()].slm.addSparkline(sl);

            //ad ogni chiave ts ho una map. questa map ha chiave int(-> "edgeid") a cui viene associata una map per associare la descrizione "meta" e la lista dei nodi.
            if (!edgeMap.containsKey(ts)) {
              //edgeMap[ts] = new Map<int, List<Node>>();

              edgeMap[ts] = new Map<int, Map>();
            }
            if (!edgeMap[ts].containsKey(edgeId)) {
              edgeMap[ts][edgeId] = new Map();
            }
            if (!edgeMap[ts][edgeId].containsKey("list")) {
              edgeMap[ts][edgeId]["list"] = new List<Node>();
            }
            if (!edgeMap[ts][edgeId].containsKey("meta")) {
              edgeMap[ts][edgeId]["meta"] = new Map();
            }

            //["list"] = new List<Node>();
            //edgeMap[ts][edgeId]["meta"]= new Map();

            edgeMap[ts][edgeId]["list"].add(nodeMap[i.toString().trim()]);
            edgeMap[ts][edgeId]["meta"]["name"] = description;
            meta = edgeMap[ts][edgeId]["meta"];
            // meta["meta"]["name"]=description;
            // meta["meta"]=arrayelement[ENTRYTAGS][TITLE];
          });
        }
      }*/
      edgeId++;
    });

    edgeMap.forEach((String ts, edgeTsMap) {
        edgeTsMap.forEach((int edgeId, Map ln) {
          if (checkEdgeValidity(ln["list"])) {
            num w = ln.length.toDouble();
            Edge newEdge = new Edge(ln["list"], w, ln["meta"]);
      //          mainGraph.addEdgeIfAbsent(newEdge, ts);
            mainGraph.addEdge(newEdge, ts);
          }
        });

    });


    tm.sortTimes();
  }

  /**
   * Parse data from csv File and sets Graph class with values
   * is a private function
   */
  void _getDataFromCsvFile(String jsonString) {
    var d = new FirstOccurrenceSettingsDetector(eols: ['\r\n', '\n', '\r'],
                                                textDelimiters: ['"', "'"]),
        csv = CsvToListConverter(csvSettingsDetector: d, shouldParseNumbers:false );
    List<List<dynamic>> res = csv.convert(jsonString).cast<List<dynamic>>();
    mainGraph.reset();
    tm.reset();

    int _EDGE_ID = 0;
    int _NODE_NAME = 1;
    int _TS = 2;
    int _EDGE_NAME = 3;
    int _COM_NAME = 4;

    Map<String, Node> nodeMap = new Map();
    Map<String, Map<String, List<Node> > > edgeMap = new Map();
    Map<String, Map<String, String > > edgeNameMap = new Map();

    int nodeCount = 0;
    bool hasEdgeName = false;

    res.forEach((row) {
        row = row.cast<String>();
        if(row.length >= 3) {
//        print(row.toString());
          String nodeName = row[_NODE_NAME];
          bool shouldAddNode = !nodeMap.containsKey(nodeName);
          if (shouldAddNode) {
            Node newNode = new Node(nodeCount);
            newNode.name = nodeName;
            //  TODO: change this, in this file format nodes don't have data associated,
            if (row.length > _COM_NAME) {
              String com = row[_COM_NAME].trim();
              if (com != 'undefined') {
                mainGraph.communities.addCommunity(com);
                newNode.community = com;
              }
            }
            newNode.slm = new SparkLineManager(canvas);
            mainGraph.nodes.add(newNode);
            nodeMap[nodeName] = newNode;
            nodeCount++;
          }
          String ts = (row[_TS]).toString();
          tm.addTime(ts);

          Sparkline sl = new Sparkline([-1.0]);
          sl.timeFrame = ts;
          sl.minValue = 0.0;
          sl.maxValue = 2.0;
          nodeMap[nodeName].slm.addSparkline(sl);

          if (row.length > _COM_NAME) {
            String com = row[_COM_NAME];

            mainGraph.communities.addCommunity(com);
            num comId = mainGraph.communities.getComId(com).toDouble();

            if (com != 'undefined') {
              Sparkline sl = new Sparkline([comId]);
              sl.timeFrame = ts;
              sl.colorMap = ColorCodings.Communities;
              nodeMap[nodeName].slm.addSparkline(sl, 'community');
            }
          }

          String edgeId = row[_EDGE_ID];

          if (!edgeMap.containsKey(ts)) {
            edgeMap[ts] = new Map<String, List<Node>>();
          }
          if (!edgeMap[ts].containsKey(edgeId)) {
            edgeMap[ts][edgeId] = new List<Node>();
          }
          edgeMap[ts][edgeId].add(nodeMap[nodeName]);
          if (row.length > 3) {
            hasEdgeName = true;
            if (!edgeNameMap.containsKey(ts)) {
              edgeNameMap[ts] = new Map<String, String>();
            }
            if (!edgeNameMap[ts].containsKey(edgeId)) {
              edgeNameMap[ts][edgeId] = row[_EDGE_NAME];
            }
          }
        }
    });

    edgeMap.forEach((String ts, edgeTsMap){
      edgeTsMap.forEach( (String edgeId, List<Node> ln){
        if (checkEdgeValidity(ln)) {
          Map meta = new Map();
          if(hasEdgeName){
            meta['name'] = edgeNameMap[ts][edgeId];
          }
          num w = ln.length.toDouble();
          Edge newEdge = new Edge(ln, w, meta);
//          mainGraph.addEdgeIfAbsent(newEdge, ts);
          mainGraph.addEdge(newEdge, ts);
        }
      });
    });
    tm.sortTimes();
  }

  /**
   * Parse data from json File and sets Graph class with values
   * is a private function
   */
  void _getDataFromJsonFile(String jsonString) {
    Map fileData = json.decode(jsonString); // gets the JSON file
    // reset to make sure graph is empty: we are about to load a new graph
    mainGraph.reset();
    tm.reset();

    //get metadata
    var metadata = fileData['metadata'];

    // TODO: only used for TESTING purposes, remove this if when
    // a correct management of such file is available
    // check if it is MarieBucher dataset
    if (metadata.containsKey('MBdataset')) {
      _parseFileFormatMB(fileData);
    }
    else {
      //check if file is the new format
      if (metadata.containsKey('format')) {
        _parseFileFormat2(fileData);
      } else {
        _parseFileFormat1(fileData);
      }
    }
  }


  void readMetaJson(Map metadata){
    if(metadata.containsKey('hyperedge_meta')){
      mainGraph.hyperedge_meta = metadata['hyperedge_meta'];
    }
    if(metadata.containsKey('node_meta')){
      mainGraph.node_meta = metadata['node_meta'];
    }
    if(metadata.containsKey('group_meta')){
      mainGraph.group_meta = metadata['group_meta'];
    }
    if(metadata.containsKey('ts_meta')){
      mainGraph.ts_meta = metadata['ts_meta'];
    }
  }

  void _parseFileFormat1(Map fileData) {
    Map metadata = fileData['metadata'];

    readMetaJson(metadata);
    // get nodes first
    var nodesFile = fileData['nodes'];
    nodesFile.forEach((nf) {
      Node newNode = new Node(int.parse(nf['id']));

      if (nf.containsKey('community')) {
        if (nf['community'] != 'undefined') {
          newNode.community = nf['community'];
          mainGraph.communities.addCommunity(nf['community']);
        }
      }

      newNode.name = nf['name']; // time series name => node name
      if (nf["pos"] != null) {
        var pos = nf["pos"];
        newNode.coordY = pos["y"];
        newNode.coordX = pos["x"];
      }
      //      newNode.name = nf['name'].toString().substring(1,nf['name'].toString().indexOf("  "));
      //      List ns = nf['name'].split(" ");
      //      newNode.name = ns[0];
      //      if (ns[0] == "")
      //      newNode.name = ns[1];
      SparkLineManager slm = new SparkLineManager(canvas);

      nf['data'].forEach((tsData) {
        Sparkline sl = _parseSparkLine(tsData, 'value');
        slm.addSparkline(sl);

        if (tsData.containsKey('community')) {

          num nodeCom = tsData['community'].length > 0 ? mainGraph.communities.getComId(tsData['community'][0]) : null;

          if(nodeCom != null) {
            String ts = tsData['ts'].toString();
            Sparkline sl = new Sparkline([nodeCom]);
            sl.timeFrame = ts;
            sl.colorMap = ColorCodings.Communities;
            slm.addSparkline(sl, 'community');

          }
        }else if (newNode.hasCommunity) {
          String ts = tsData['ts'].toString();
          Sparkline sl = new Sparkline([mainGraph.communities.getComId(newNode.community).toDouble()]);
          sl.timeFrame = ts;
          sl.colorMap = ColorCodings.Communities;
          slm.addSparkline(sl, 'community');
        }

      });
      if (metadata['wavelets'] == 1) {
        nf['data'].forEach((tsData) {
          Sparkline sl = _parseSparkLine(tsData, 'wavelets');
          sl.colorMap = ColorCodings.WavSet3;
          slm.addSparkline(sl, 'wavelets');
        });
      }

      newNode.slm = slm;
      mainGraph.nodes.add(newNode);
      // the variable type should be checked (double, int, ...)
      if (mainGraph.maxNodeValue < newNode._value)
        mainGraph.maxNodeValue = newNode._value;
    });

    var edgesFile = fileData['edges'];
    var nodeMap = new Map();
    mainGraph.nodes.forEach((n) {
      nodeMap.putIfAbsent(n.id, () => n);
    });
    int counter = 0;
    edgesFile.forEach((ef) {
      String ts = ef['ts'].toString();
      //      if(DEBUG) print("ts " + ts);

      tm.addTime(ts);

      ef['list'].forEach((l) {
        List<Node> ln = new List<Node>();
        l['ids'].forEach((value) {
          ln.add(nodeMap[int.parse(value)]);
        });
        // nodeMap causes null values if the node of edge is not found that
        // led to error
        if (checkEdgeValidity(ln)) {
          num w = ln.length.toDouble();
          if (l.containsKey('w')) l['w'] is double ? l['w'] : l['w'].toDouble();
          Map meta = new Map();
          if(l.containsKey('meta')) meta = l['meta'];
          if(l.containsKey('id')) meta['hal_docid'] = l['id'].toString();
          Edge newEdge = new Edge(
              ln, w, meta);
//          mainGraph.addEdgeIfAbsent(newEdge, ts);
          mainGraph.addEdge(newEdge, ts);
        }
        counter++;
      });

    });
    //print(counter.toString() + " edges readed!");
    tm.sortTimes();
  }

  void _parseFileFormat2(Map fileData) {

    print('reading');

    var metadata = fileData['metadata'];
    readMetaJson(metadata);


    var nodesFile = fileData['nodes'];

    nodesFile.forEach((nf) {
      Node newNode = new Node(int.parse(nf['id']));
      newNode.name = nf['name'];
      if (nf["pos"] != null) {
        var pos = nf["pos"];
        newNode.coordY = pos["y"];
        newNode.coordX = pos["x"];
      }

      if (nf.containsKey('community')) {
        if (nf['community'] != 'undefined') {
          newNode.community = nf['community'];
          mainGraph.communities.addCommunity(nf['community']);
        }
      }

      //  TODO: change this, in this file format nodes don't have data associated,
      // so it doesn't make sense to have a sparkline per each node
      // but we need to render things properly
      newNode.slm = new SparkLineManager(canvas);
      mainGraph.nodes.add(newNode);
//      // the variable type should be checked (double, int, ...)
//      if (mainGraph.maxNodeValue < newNode._value)
//        mainGraph.maxNodeValue = newNode._value;

    });

//    num comId = 1.0;

//    Map<String, num> communitiesMap = new Map<String, num>();
//
//    mainGraph.nodes.forEach((n) {
//      if (n.hasCommunity){
//        int nCom = int.parse(n.community, onError: (source) => null);
//          if(nCom != null){
//            if (!communitiesMap.containsKey(n.community)) {
//              communitiesMap[n.community] = nCom.toDouble();
//              //          comId++;
//            }
//          }
//          else
//            communitiesMap[n.community] = 0.0;
//      }
//    });


    var edgesFile = fileData['edges'];
    var nodeMap = new Map();
    mainGraph.nodes.forEach((n) {
      nodeMap.putIfAbsent(n.id, () => n);
    });

    edgesFile.forEach((l) {
      String ts = l['ts'].toString();
      tm.addTime(ts);

      List<Node> ln = new List<Node>();
      l['ids'].forEach((nid) {
        Node node = nodeMap[int.parse(nid)];
        ln.add(node);

        if (node.hasCommunity) {
          Sparkline sl = new Sparkline([mainGraph.communities.getComId(node.community).toDouble()]);
          sl.timeFrame = ts;
          sl.colorMap = ColorCodings.Communities;
          node.slm.addSparkline(sl, 'community');
        }

        if (!node.slm.sparkLines.containsKey(ts)) {
          Sparkline sl = new Sparkline([-1.0]);
          sl.timeFrame = ts;
          sl.minValue = 0.0;
          sl.maxValue = 2.0;
          node.slm.addSparkline(sl);
        }
      });
      // nodeMap causes null values if the node of edge is not found that
      // led to error
      if (checkEdgeValidity(ln)) {
        num w = ln.length.toDouble();
        if (l.containsKey('w')) l['w'] is double ? l['w'] : l['w'].toDouble();
//        print("sono in _parseFileFormat2");
        Edge newEdge = new Edge(ln, w, l['meta']);
//          mainGraph.addEdgeIfAbsent(newEdge, ts);
        mainGraph.addEdge(newEdge, ts);
      }
    });
    tm.sortTimes();
  }

  /// parser per Marie Boucher csv2json dataset
  void _parseFileFormatMB(Map fileData) {
//    var edgesInFile = fileData['edges'];
    var nodesFile = fileData['edges'];
    Map nodes = {};
    var length = 0;

//    edgesInFile.forEach((ef) {
    print("scanning edges...");
    nodesFile.forEach((ef) {
      if (nodes.isNotEmpty) length = nodes.length;
      if (!nodes.containsKey(ef['Nom1'])) {
        nodes.putIfAbsent(ef['Nom1'].toString(), () => length);
      }
      if (!nodes.containsKey(ef['Nom2'])) {
        nodes.putIfAbsent(ef['Nom2'].toString(), () => length);
      }
    });

    // allows for iteration in the map
    void assignNodesToMainGraph(key, value) {
      Node newNode = new Node(value);
      newNode.name = key.toString();
      newNode.slm = new SparkLineManager(canvas);
      mainGraph.nodes.add(newNode);
      //print ("the node " + newNode.value.toString() + " - " + newNode.name.toString() + " has been inserted");
    }

    //insert all nodes in the map to mainGraph
    nodes.forEach(assignNodesToMainGraph);

    // communities management
    num comId = 1.0;
    Map<String, num> communitiesMap = new Map<String, num>();
    mainGraph.nodes.forEach((n) {
      if (n.hasCommunity)
        if (!communitiesMap.containsKey(n.community)) {
          communitiesMap[n.community] = comId;
          comId++;
        }
    });

    //iterate again into edges to assign hyperedges
//    nodesFile.forEach((l) {
    for (final l in nodesFile) {
      //search for the TS associated to the edge
      String ts = l['Date'].toString();
      ts = ts.substring(
          ts.length - 4); //takes last 4 digit, only year is of interest
      tm.addTime(ts);

      List<Node> ln = new List<Node>();

      addNodeToListOfNodes(ln, ts, l['Nom1'], communitiesMap);
      addNodeToListOfNodes(ln, ts, l['Nom2'], communitiesMap);

      Node node = new Node(mainGraph.nodes.getIdFromName(l['Nom1']));
      node.slm = new SparkLineManager(canvas);
      ln.add(node);
      if (null == node.slm || !node.slm.sparkLines.containsKey(ts)) {
        Sparkline sl;

        if (node.hasCommunity) {
          sl = new Sparkline([communitiesMap[node.community]]);
          sl.colorMap = ColorCodings.Communities;
        }
        else
          sl = new Sparkline([0.0]);

        sl.timeFrame = ts;
        sl.minValue = 0.0;
        sl.maxValue = 2.0;
        node.slm.addSparkline(sl);
      }
//      });

      // nodeMap causes null values if the node of edge is not found that
      // led to error
      if (checkEdgeValidity(ln)) {
        num w = 0.0;
        if (l.containsKey('w')) l['w'] is double ? l['w'] : l['w'].toDouble();
        Edge newEdge = new Edge(ln, w);
//          mainGraph.addEdgeIfAbsent(newEdge, ts);
        mainGraph.addEdge(newEdge, ts);
      }
    } //);
    tm.sortTimes();
  }

  void addNodeToListOfNodes(List ln, String ts, String nodeName,
      Map communitiesMap) {
    Node node = new Node(mainGraph.nodes.getIdFromName(nodeName));
    ln.add(node);

    if (null == node.slm || !node.slm.sparkLines.containsKey(ts)) {
      Sparkline sl;
      if (node.hasCommunity) {
        sl = new Sparkline([communitiesMap[node.community]]);
        sl.colorMap = ColorCodings.Communities;
      } else {
        sl = new Sparkline([0.0]);
      }
      sl.timeFrame = ts;
      sl.minValue = 0.0;
      sl.maxValue = 2.0;

      if (null != node.slm) {
        node.slm.addSparkline(sl);
      } else {
        node.slm = new SparkLineManager(canvas);
      }
    }
  }

///////////////////////

  Sparkline _parseSparkLine(tsData, String field) {
    List<double> timePointSet = new List<double>();

    bool first = true;
    double maxValue;
    double minValue;

    tsData[field].forEach((tempPoint) {
      double timePoint = (tempPoint is double) ? tempPoint : tempPoint
          .toDouble();
      if (first) {
        maxValue = timePoint;
        minValue = timePoint;
        first = false;
      } else {
        if (maxValue < timePoint) maxValue = timePoint;
        if (minValue > timePoint) minValue = timePoint;
      }
      timePointSet.add(timePoint);
    });

    Sparkline sl = new Sparkline(timePointSet);
    String ts = tsData['ts'].toString();
    sl.timeFrame = ts;
    if (sl.maxValue < maxValue) sl.maxValue = maxValue;
    if (sl.minValue > minValue) sl.minValue = minValue;
    return sl;
  }

  bool checkEdgeValidity(List<Node> ln) {
    bool isValid = true;
    ln.forEach((n) {
      if (null == n) isValid = false;
    });
    return isValid;
  }
}