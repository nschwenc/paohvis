import 'package:event_bus/event_bus.dart';

/// The global [EventBus] object.
EventBus eventBus = new EventBus();

/// TimeSliderChanged
class TimeSliderChanged {
  String text;
  TimeSliderChanged(this.text);
}

class NodeFilterChanged {
  String text;
  NodeFilterChanged(this.text);
}

class NodeSliderChanged {
  num delta;
  NodeSliderChanged(this.delta);
}

class NodesScrolled{
  num delta;
  NodesScrolled(this.delta);
}

class VertScrollNodeSpaceChanged{
  num n0;
  num n1;
  VertScrollNodeSpaceChanged(this.n0, this.n1);
}

class VertScrollVisLimitsChanged{
  num x0;
  num v0;
  num v1;
  VertScrollVisLimitsChanged(this.v0, this.v1, this.x0);
}

class TimeScrollTsSpaceChanged{
  num x0;
  num x1;
  TimeScrollTsSpaceChanged(this.x0, this.x1);
}

class TimeScrollVisLimitsChanged{
  num x0;
  num v0;
  num v1;
  TimeScrollVisLimitsChanged(this.v0, this.v1, this.x0);
}

class UpdateEdgeHighlights{
  List<num> list;
  UpdateEdgeHighlights(this.list);
}

class UpdateNodeHighlights{
  List<num> list;
  UpdateNodeHighlights(this.list);
}

class UpdateEdgeSelects{
  List<num> list;
  UpdateEdgeSelects(this.list);
}

class UpdateNodeSelects{
  List<num> list;
  UpdateNodeSelects(this.list);
}

class TooltipChanged {
  bool visible;
  String text;
  num x;
  num y;
  TooltipChanged(this.text, this.x, this.y);
}

class DataSetChanged {
  bool visible;
  String name;
  String description;
  DataSetChanged(this.name, this.description);
}

class TimeSlotsChanged{
//  String text;
  TimeSlotsChanged();
}

class TimeSlotsScrolled{
  int delta;
  TimeSlotsScrolled(this.delta);
}

class TimeSlotsMoved{
  num posX;
  TimeSlotsMoved(this.posX);
}

class TimeSlotsStatisticsChanged{
  String stat;
  TimeSlotsStatisticsChanged(this.stat);
}

class NodeStatisticsChanged{
  String stat;
  NodeStatisticsChanged(this.stat);
}




