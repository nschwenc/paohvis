part of paohvis;

class Writer{
  Graph _graph;

  Writer (Graph g){
    _graph = g;
  }
  void createFile(AnchorElement link, String fileName){

    Map mapMetadata = createMetadata(fileName);
    List mapNodes = createNodes();
    List mapEdges = createEdges();

    Map mapData = new Map();
    mapData['metadata'] = mapMetadata;
    mapData['nodes'] = mapNodes;
    mapData['edges'] = mapEdges;

    List data = new List();
    data.add(json.encode(mapData));

// Plain text type, 'native' line endings
    var blob = new Blob(data, 'text/plain', 'native');
    var url = Url.createObjectUrlFromBlob(blob).toString();

    link
      ..href = url
      ..download = fileName;
  }

  List createNodes(){
    List listNodes = new List();
    _graph.nodes.valid.forEach((Node n){
      Map mapNode = new Map();
      mapNode['id'] = n.id.toString();
      mapNode['name'] = n.name;
      if(n.coordX != null && n.coordY != null){
        Map mapPos = new Map();
        mapPos['x'] = n.coordX;
        mapPos['y'] = n.coordY;
        mapNode['pos'] = mapPos;
      }
      mapNode['name'] = n.name;
      //TODO: community
      listNodes.add(mapNode);
    });
    return listNodes;
  }

  List createEdges(){
    List listEdges = new List();
    _graph.edges.forEach((String ts, List<Edge> tsEdges){
      tsEdges.forEach((Edge e){
        Map mapEdge = new Map();
        mapEdge['ts'] = ts;
        mapEdge['ids'] = new List();
        mapEdge['meta']=e._meta;
        e.forEach((var n){
          mapEdge['ids'].add(n.id.toString());
        });
        listEdges.add(mapEdge);
      });
    });
    return listEdges;
  }

  Map createMetadata(String dataName){
    var mapMetadata = new Map();
    mapMetadata['format'] = 'time_flat';
    mapMetadata['name'] = dataName;
    return mapMetadata;
  }
}
