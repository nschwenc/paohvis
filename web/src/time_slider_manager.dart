part of paohvis;

class TimeSliderManager{

  TimeManager tm = new TimeManager();

  RangeInputElement _element;
  DivElement _labelsElement;

  SpanElement _minTlElement;
  SpanElement _maxTlElement;

  int _width;
  var onInputSlider;

  TimeSliderManager(RangeInputElement element, [DivElement labelsElement = null, SpanElement minTl = null, SpanElement maxTl = null]){
    _element = element;
    _width = _element.clientWidth;
    _labelsElement = labelsElement;

    _minTlElement = minTl;
    _maxTlElement= maxTl;

    onInputSlider = _element.onInput.listen((Event e) {
      eventBus.fire(new TimeSliderChanged(_element.value));
//      eventBus.fire(new TimeSlotsChanged());
    });
  }


  void update(val){
    _element.value = val;
  }

  void setup(){

    _element.max = (tm.numberOfTimeSlots-1).toString();

    double step = ((_width)/( double.parse(_element.max) - double.parse(_element.min) ) );
    double cp = 0.0;

    if(_labelsElement != null){
      _labelsElement.children.clear();

      tm.timeList.forEach((ts){
        DivElement tsLabel = new DivElement();
        tsLabel.className = "tsLabel";
        tsLabel.text = ts;
        tsLabel.style.left = (cp.round()).toString()+'px';
        _labelsElement.children.add(tsLabel);
        cp += step;
      });
//      print(_element.attributes.keys);
    }
    if(_minTlElement != null){
      String minTs = tm.timeList.first;
      _minTlElement.text = minTs;
    }
    if(_maxTlElement != null){
      String maxTs = tm.timeList.last;
      _maxTlElement.text = maxTs;
    }
  }


}