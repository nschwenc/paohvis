part of paohvis;

class CommunityLegendManager {
  DivElement _element;
//  Communities _communities = null;
  ColorManager _cm = new ColorManager();

  num _width;
  bool mouseDown = false;
  num pos0x, pos0y, pos1x, pos1y;
  var _onMouseUp, _onMouseMove, _onMouseDown;

  CommunityLegendManager([DivElement element = null]) {
    _element = element;
    _width = _element.clientWidth;

    if (_element != null) {
      Draggable draggable = new Draggable(_element,
          avatarHandler: new AvatarHandler.original(), handle: '.legendTitle');
    }
  }

  DivElement getComElement(Communities com, String ts) {
    int id = com.getComId(ts);
//      print(ts + " " + id.toString());
    DivElement sqColor = new DivElement();
    sqColor.style.width = '15px';
    sqColor.style.height = '15px';
    sqColor.style.marginRight = '3px';
    sqColor.style.backgroundColor = _cm.getCommunityColor(id, false);

    DivElement tsLabel = new DivElement();
    tsLabel.className = "comLabel";
    tsLabel.text = ts;
    //          tsLabel.style.top = (cp.round()).toString() + 'px';
    //          tsLabel.style.left = '20px';

    DivElement legendCom = new DivElement();
    legendCom.className = "legendCom";
    legendCom.children.add(sqColor);
    legendCom.children.add(tsLabel);
    //          legendCom.style.top = ((id*step).round()).toString() + 'px';
    return legendCom;
  }

  void render(Communities com, [List<String> order = null]) {
    if (_element != null) {
      _element.children.clear();
      _element.style.display = "none";

      if (!(ENABLED_COLORGROUP || HEATMAP)) return;

      if (com != null) {
//        num cp = 0;
        if (order == null) {
          order = com.getCommunitiesNames();
          order.sort();
        }

        if (order.length > 0) {
          DivElement sqColorOthers = new DivElement();
          sqColorOthers.style.width = '15px';
          sqColorOthers.style.height = '15px';
          sqColorOthers.style.marginRight = '3px';
          sqColorOthers.style.backgroundColor = 'black';

          DivElement tsLabelOthers = new DivElement();
          tsLabelOthers.className = "comLabel";
          tsLabelOthers.text = 'Others';

          _element.style.display = "flex";
          DivElement titleDiv = new DivElement();
          titleDiv.className = "group-change legendTitle";
          titleDiv.text = 'Origin:';
          _element.children.add(titleDiv);
          DivElement comOthers = new DivElement();
          comOthers.className = "legendCom";
          comOthers.children.add(sqColorOthers);
          comOthers.children.add(tsLabelOthers);
          _element.children.add(comOthers);
        }

        order.forEach((ts) {
          DivElement legendCom = getComElement(com, ts);
//          legendCom.style.width = '45%';
          _element.children.add(legendCom);
        });
//        DivElement legendCom = getComElement(com, order.);
////          legendCom.style.width = '45%';
//        _element.children.add(legendCom);
      }
    }
  }
}
