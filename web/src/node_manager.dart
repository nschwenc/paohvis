part of paohvis;

// list of nodes to be displayed
// this should take care of only visual properties
class NodeManager {
  CanvasElement _canvas;
  CanvasRenderingContext2D _context;
  ColorManager cm = new ColorManager();
  Graph _graph;

  Nodes nodes = new Nodes();
  num _topLeftX = 0; // x position of the drawing area
  num _topLeftY = 0; // y position of the drawing area
  int canvasWidth = canvas.width;
  int canvasHeight = canvas.height;
  num _frameWidth = 0;
  num _frameHeigth = 0;
  num margin = 0;
  num _nodeHeight = 0;
  num _nodeWidth = 0;

  num _leftMargin = 0; //used for give a left margin when render sparklines

  num _minY = double.infinity;
  num _maxY = double.negativeInfinity;

  int selectedNodesNum = 0; //count the number of selected nodes for stats purposes
  int highlightedNodesNum = 0; //count the number of highlighted nodes for stats purposes


  // label variables
  int fontWidth = 0;
  int _fontHeight = 0;
  num labelMargin = 0;
  num labelWidth = 0;

  List<String> backColors = Theme.ALT_COLORS;

//  int sparkLineWidth;

  // in case someone needs this
  num get frameWidth  => _frameWidth;
  num get frameHeight => _frameHeigth;
  num get nodeHeight  => _nodeHeight;

  set frameWidth(num value){
    _frameWidth = value;
    NODES_FRAME_WIDTH = _frameWidth; // TODO: this global shouldn't be modified here
  }

  NodeManager(Graph g, CanvasElement canvasElement, [num xv=0, num yv=0]){
    _graph = g;
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    nodes = g.nodes;
    _topLeftX = xv;
    _topLeftY = yv;
    setup();
  }


  // set nodelist panel position and dimensions
  // useless if such constraints are put in setup procedure useful for refreshing screen/resizing
  void setContextConstraints(num newX, num newY) {
    _topLeftX = newX;
    _topLeftY = newY;
  }

  void setLeftMargin(num lm) {
    _leftMargin = lm;
  }

  void setup(){

    _nodeHeight = SCALE*NODE_HEIGHT_DIM; //SCALE*= height/(NODE_HEIGHT_DIM*n)
    _nodeWidth = _nodeHeight;
    _fontHeight = min((_nodeHeight * 1.75).round(), MAX_FONT);

    computeLabelWidth();

    frameWidth = _nodeWidth + labelWidth + labelMargin;
    if(DRAW_SQUARE) {
      frameWidth += _nodeWidth;
    }

//    Rectangle visibleArea = new Rectangle(0, 0, _frameWidth, canvas.height);

    updatePositions();
  }


  updatePositions(){
    // assigns coordinates to each node


    nodes.forEach((var n){
      if(n.isValid || n.toBeValid) {
//        n.x = _topLeftX + _nodeHeight ~/ 2 + labelMargin + labelWidth;
        n.x = _topLeftX;
        n.y = (_topLeftY + _nodeHeight + n.position * 2 * _nodeHeight -
            _nodeHeight / 2).round();
        n.width = _nodeWidth; // former baseUnit.round()
        n.height = _nodeHeight;
        n.margin = n.height ~/ 2;
        n.slm.setup();

        _maxY = max(_maxY, n.y);
        _minY = min(_minY, n.y);

      }
    });

  }

  updateMinMax(){

    _minY = double.infinity;
    _maxY = double.negativeInfinity;

    nodes.valid.forEach((n) {
      _minY = min(_minY, n.y);
      _maxY = max(_maxY, n.y);
    });
  }

  setOffset(num offset){
    if (((_minY-_nodeHeight/2) + offset) > _topLeftY) //return
      offset = _topLeftY - _minY + _nodeHeight/2;

    nodes.valid.forEach((n) {
//      if(n.isValid) {
        n.y += offset;
//      }
    });
//    nodes.updateVisibility(_canvas);
  }

  /**
   * computation to reserve space for labels
   * it changes the _frameWidth that is the space available for the visualization
   * */
  void computeLabelWidth() {

    if (NODE_LABELS) {
      String prevFillStyle = _context.fillStyle;
      String prevFont = _context.font;

      _context
        ..font = (_fontHeight).toString() + "px " + CANVAS_FONT;

      num maxNameLength = 0.0;
      String longestName = '';
      String nDegree = '';
      if(SHOW_NODE_DEGREE) nDegree = ' (00)';

      nodes.forEach((n) {
        num nameLength = _context.measureText(n.name + nDegree).width;
        if (nameLength > maxNameLength) {
          longestName = n.name;
          maxNameLength = nameLength;
        }
      });

//      longestName = 'Jacques Souchay et Bouchad';

      labelMargin = 5; // for aesthetics, TODO: shouldn't be hardcoded
      labelWidth = (_context.measureText(longestName + nDegree).width + labelMargin).toDouble();

      _context.fillStyle = prevFillStyle;
      _context.font = prevFont;

    } else {
      labelWidth = 0;
      labelMargin = 0;
    }

  }

  // draws node label
  void drawLabel(Node n) {
    if (NODE_LABELS) {
      String prevFillStyle = _context.fillStyle;

      _context
        ..fillStyle = cm.getNodeColor(n)
        ..font = "lighter " + (_fontHeight-2).toString() + "px " + CANVAS_FONT;

      if (n.isSelected) {
        _context
          ..font = "bold " + (_fontHeight).toString() + "px " + CANVAS_FONT;
      }else if (n.isAdjacentToHighlighted) {
        _context
          ..font = (_fontHeight).toString() + "px " + CANVAS_FONT;
      }else if (n.isHighlighted) {
        _context
          ..font = (_fontHeight).toString() + "px " + CANVAS_FONT;
//          ..font = "bold " + (_fontHeight).toString() + "px " + CANVAS_FONT;
      }
      String nodeText = n.name + (SHOW_NODE_DEGREE ? ' (' + n.degree.toString() + ')' : '');
      _context
        ..textBaseline="middle";
      _context
        ..fillText(
            nodeText,
            labelMargin+n.x,
            (n.y + n.height/2))
        ..fillStyle = prevFillStyle;
    }
  }

  void drawAggregatedValue(Node n) {
    // draws node label
    if (NODE_AGGREGATED) {
      var prevFillStyle = _context.fillStyle;
      // draws aggregated
      _context
        ..fillStyle = getColorCode(0.0, 3 * nodes.maxAverageValue, n.aggregatedValue,
            ColorCodings.grayscale, false, 120, new HexColor("#eeeeee"));

      _context
        ..beginPath()
        ..rect(labelMargin+n.x, n.y - n.height/2, labelWidth, n.height*2)
        ..closePath()
        ..fill();

      _context.fillStyle = prevFillStyle;
    }
  }

  // nodes panel
  void renderNodes() {
    _context
      ..fillStyle = Theme.verticesFillMainContent
      ..strokeStyle = Theme.verticesBorderMainContent
      ..globalAlpha = 1;

    _context
      ..beginPath()
      ..rect(_topLeftX, _topLeftY, _frameWidth, _frameHeigth)
      ..closePath()
      ..fill()
      ..stroke();

    nodes.visible.forEach((n) {
      if(!n.isValid || !n.toBeValid) return;

//      if(n.isValid) {
//        if (n.isVisible) {
          // defines background and foreground color according to the status

      String nodeColor = '';
      if((HEATMAP  ) && n.hasCommunity) {
        nodeColor = cm.getCommunityColor(_graph.communities.getComId(n.community));
         _context.globalAlpha = 0.5;
      }else{
         nodeColor =  backColors[(n.position/N_ALTERNATE_ROWS).floor()%2];
      }

      _context
        ..fillStyle = nodeColor
        ..strokeStyle = nodeColor;

      _context
        ..beginPath()
        ..rect(n.x, n.y-n.height/2, _frameWidth-4, n.height*2-2)
        ..closePath()
        ..fill()
        ..stroke();

      if(HEATMAP && n.hasCommunity) { //reset alpha
        _context
          ..globalAlpha = 1;
      }



        if(DRAW_SQUARE) {
            _context
  //          ..fillStyle = getColorCode(
  //              minNodeValue, maxNodeValue, n._value, ColorCodings.fixed, false,
  //              220, new HexColor("#ffffff"))
              ..fillStyle = Theme.fillVerticesSymbol
              ..strokeStyle = Theme.borderVerticesSymbol;

            if (n.isAdjacentToSelected) {
              _context
                ..fillStyle = Theme.adjacentVerticesSymbolFillSelect
                ..strokeStyle = Theme.adjacentVerticesSymbolBorderSelect;
            }

            if (n.isAdjacentToHighlighted) {
              _context
                ..fillStyle = Theme.adjacentVerticesSymbolFillHighlight
                ..strokeStyle = Theme.adjacentVerticesSymbolBorderHighlight;
            }

            if (n.isHighlighted) {
              _context
                ..fillStyle = Theme.verticesSymbolFillHighlight
                ..strokeStyle = Theme.verticesSymbolBorderHighlight;
            }

            if (n.isSelected) {
              _context
                ..fillStyle = Theme.verticesSymbolFillSelect
                ..strokeStyle = Theme.verticesSymbolBorderSelect;
            }

            _context
              ..beginPath()
              ..rect(n.x, n.y, n.width, n.height)
              ..closePath()
              ..fill()
              ..stroke();
        }

//        drawAggregatedValue(n);
        drawLabel(n);
//        }
//      }
    });

  }

  num getHeightAll(){
    num slHeight = 2;
    nodes.forEach((n) {
      // future implementations may require changes here
      slHeight = slHeight + (n.height * 2);// - 2.0;
    });
    return slHeight;
  }

  void renderSparklines(){
    nodes.visible.forEach((n) {

      if(!n.isValid || !n.toBeValid) return;
        // future implementations may require changes here
        num slHeight = n.height * 2;// - 2.0;
//        n.slm.renderAllSparkLines(slHeight, n);
        n.slm.renderSparkLines(slHeight, n, _leftMargin, 0, 'community');
    });

  }


}