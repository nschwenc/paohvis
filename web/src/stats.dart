part of paohvis;

abstract class Stats {

  num _topMargin = 2.0;
  num _leftMargin = 4.0;
  num _rightMargin = 4.0;
  num _bottomMargin = 1.0;

  num _topCorner = 0;
  num _leftCorner = 0;

  num _tabHeight = 0;

  CanvasElement _canvas;
  CanvasRenderingContext2D _context;

  num _height;
  num _width;


  List<num> _stats = new List<num>();
  List<num> _statsHigh = new List<num>();
  List<num> _statsSelect = new List<num>();
  List<num> _pos = new List<num>();
  String _statsName = '';

  num _min;
  num _max;

  num _visLims0 = 0;
  num _visLims1 = 0;

  num _fontSize = 18.0;
  num _currFontSize;

  num get height  => _height;
  num get width  => _width;
  String get statsName  => _statsName;
  List<num> get stats => _stats;
  List<num> get statsHigh => _statsHigh;
  List<num> get statsSelect => _statsSelect;
  List<num> get pos => _pos;

  set statsHigh(List<num> value) {
    _statsHigh = value;
  }
  set statsSelect(List<num> value) {
    _statsSelect = value;
  }

  //optional parameter tabHeight is used in tabs feature
  Stats(num h, num w, CanvasElement canvas, [num tabHeight = 0]){

    _height = h-_topMargin-_bottomMargin;
    _width = w - _leftMargin - _rightMargin;

    _canvas = canvas;
    _context = canvas.getContext('2d');

    _currFontSize = _fontSize;

    _tabHeight = tabHeight;

  }

  updateStatsPos(String statsName, List<num> stats, List<num> pos){
//    if(stats.length != pos.length) return;
    _updateStats(statsName, stats);
    _pos = pos;
    render();
  }

  updateStatsHeight(num h){
//    if(stats.length != _pos.length) return;
    _height = h-_topMargin-_bottomMargin;
    render();
  }

  updateStatsWidth(num w){
//    if(stats.length != _pos.length) return;
    _width = w - _leftMargin - _rightMargin;
    render();
  }

  _updateStats(String statsName, List<num> stats){
//    if(stats.length != _pos.length) return;
    _statsName = statsName;
    _stats = stats;

    _max = _stats.reduce(max);
    _min = _stats.reduce(min);

  }

  updatePos(List<num> pos){
//    if(pos.length != _pos.length) return;
    _pos = pos;
    render();
  }

  bool posInVisibleArea(num p){
    return true;
  }

  renderBar(num st, num pos){

  }

  renderBack(){
//    _context.fillStyle = "#ffffff";
    _context.fillStyle = Theme.statsBackground;

    _context
      ..fillRect(_leftCorner, _topCorner+_tabHeight, _width+_leftMargin+_rightMargin, _height+_topMargin+_bottomMargin);
  }

  renderBars(List<num> rStats){
    for(int i = 0; i<_stats.length; i++){
      if(posInVisibleArea(_pos[i])){
        renderBar(rStats[i], _pos[i]);
      }
    }
  }

  renderLabel(num st, num pos){
    if (st==0) return;
    _context
      ..fillText(st.toStringAsFixed(0),
          _leftMargin + _width,
          pos);
  }

  //**
  // renderLabels: renders labels in stats bars
  // */
  renderLabels(List<num> rStats){
    String fontStyle = "lighter " + (_currFontSize-2).toString() + "px " + CANVAS_FONT;
    _context
      ..fillStyle = Theme.statsLabels
      ..font = fontStyle;
    
    for(int i = 0; i<rStats.length; i++){
      if(posInVisibleArea(_pos[i]) && rStats[i] > 0){
          renderLabel(rStats[i], _pos[i]);
      }
    }

  }

  renderStats(List<num> rStats){

    if(_pos.length != rStats.length) return;

    renderBars(rStats);
    renderLabels(rStats);
  }

  render(){

    if(!DEMO_VIS4DH) {
      renderBack();

      if (statsHigh != null && statsHigh.length == pos.length) {
        _context.fillStyle = Theme.statsLabelsBackground; //"#eeeeee";
        renderBars(_stats);

        _context.fillStyle = Theme.statsLabelsForeground;
        renderStats(statsHigh);
      } else if (statsSelect != null && statsSelect.length == pos.length) {

        _context.fillStyle = Theme.statsLabelsBackground;
        renderBars(_stats);

        _context.fillStyle = Theme.statsLabelsForeground;
        renderStats(statsSelect);
      } else {
        _context
          ..fillStyle = Theme.statsLabelsForeground;
        renderStats(_stats);
      }
    }


  }



}

class StatsTop extends Stats{

//  num _barWidth = 0;

  //optional parameter tabHeight is used in tabs feature
  StatsTop(num h, num w, CanvasElement canvas, [num tabHeight = 0])
      : super(h, w, canvas, tabHeight){
  }

  bool posInVisibleArea(num p){
    return (p>=_leftCorner && p < _width);
  }

  renderLabel(num st, num pos) {
    if(st==0) return;
    int fix = 0;
    if(st != st.round()) fix = 2;
    _context
      ..fillText(st.toStringAsFixed(fix),
          pos + _leftMargin + 1, _topMargin + _tabHeight + _height - _currFontSize / 2);
  }
  renderBar(num st, num pos){

    num barHeight = scaleCoord(st, 0, _max, 0, _height-_tabHeight-2);

    _context
      ..fillRect(pos+_leftMargin, _height-barHeight+_topMargin+_tabHeight, _currFontSize, barHeight);
  }
  renderStats(List<num> rStats) {
    _currFontSize = _fontSize;
    if (_pos.length > 2) {
      _currFontSize = min(_fontSize, (_pos[1] - _pos[0]-5).abs());
    }

    super.renderStats(rStats);
  }


}

// list of nodes to be displayed
class StatsLeft extends Stats{

  StatsLeft(num h, num w, CanvasElement canvas)
      : super(h, w, canvas){

  }

  bool posInVisibleArea(num p){
    return (p>=_topCorner && p < _height);
  }

  renderBar(num st, num pos){

    num barWidth = scaleCoord(st, 0, _max, 0, _width);

    _context
      ..fillRect(_width-barWidth+_leftMargin,pos-_currFontSize/2+1, barWidth, _currFontSize-2);
  }

  renderLabels(rStats){

    String prevAlign = _context.textAlign;
    _context.textAlign = "right";

    super.renderLabels(rStats);

    _context.textAlign = prevAlign;

  }

  renderStats(List<num> rStats) {
    _currFontSize = _fontSize;
    if (_pos.length > 2) {
      _currFontSize = min(_fontSize, (_pos[1] - _pos[0]).abs());
    }

    super.renderStats(rStats);
  }

}