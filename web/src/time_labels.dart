part of paohvis;

// list of nodes to be displayed
class TimeLabels {

  CanvasElement _canvas;
  CanvasRenderingContext2D _context;

  TimeManager tm = new TimeManager();
  ColorManager cm = new ColorManager();
  num _topMargin = 0;
  num _bottomMargin = 5;
  num _fontSize = MAX_FONT;
  num _labelMargin = 4;
  num _height = 0;
  num _width = 0;


  TimeLabels(CanvasElement canvasElement, [num yv = 0, num width]) {
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    _topMargin = yv;
    _height = _fontSize + _bottomMargin;
    _width = width;

    if(DEMO_VIS4DH){
      _fontSize = MAX_FONT;
    }
  }

  int get numTimes  => tm.numTimes;
  num get height  => _height;

  // renders labels
  renderLabels(){
    // background frame
    String prevFillStyle = _context.strokeStyle;
    _context
      ..fillStyle = Theme.tsFillMainContent
      ..strokeStyle = Theme.tsFillMainContent;

    _context
      ..beginPath()
      ..rect(0, _topMargin, _width, _height)
      ..closePath()
      ..fill()
      ..stroke();

    num labelMaxWidth;
    if (TIME_LABELS) {
      tm.forEach((var s){
        String fontStyle = "lighter " + (_fontSize-2).toString() + "px " + CANVAS_FONT;
        if(tm.isVisible(s)){
          if(!tm.isMinimized(s)) {
            labelMaxWidth = tm.getWidth(s);
            num pos = tm.getPosX1(s) + _labelMargin;
            if (tm.isSelected(s)) {
              fontStyle =
                  "bold " + (_fontSize).toString() + "px " + CANVAS_FONT;
            } else if (tm.isHighlighted(s)) {
              fontStyle = (_fontSize).toString() + "px " + CANVAS_FONT;
            }
            _context
              ..fillStyle = cm.getTsColor(tm, s)
              ..font = fontStyle
              ..fillText(s,
                  pos,
                  _topMargin + _fontSize - _bottomMargin,
                  labelMaxWidth - _labelMargin);
          }
        }
      });
    }
    _context
        ..fillStyle = prevFillStyle;
  }
}
