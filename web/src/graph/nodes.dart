part of paohvis;

// list of nodes to be displayed
class Nodes  extends IterableBase{

  List <Node> nodes = new List<Node>();
  Map<int, int> nodesId2Pos = new Map<int, int>();

  double maxNodeValue; // assuming that node values are always double, might not be the case
  double minNodeValue;
  double maxAverageValue;
  //[min|max]DataValue is useful to normalize data in the Visualization
  double maxDataBaseValue;
  double minDataBaseValue;

  num coordXMin = 0;
  num coordXMax = 0;
  num coordYMin = 0;
  num coordYMax = 0;

  Nodes(){
    nodes = new List<Node>();
    initValues();
  }

  int getIdFromName (String namestr){
    int id=-1;
    for (final n in nodes){
      print ("check " + n.name + " vs $namestr");
      if (n.name==namestr) {
        id=n.id;
        break;
      }
    }
    return id;
  }

  Iterator<Node>  get iterator {
    return nodes.iterator;
  }

  Iterable<Node> get valid{
    return nodes.where((n) => n.isValid);
  }

  Iterable<Node> get invalid{
    return nodes.where((n) => !n.isValid);
  }

  Iterable<Node> get visible{
    return nodes.where((n) => n.isVisible);
  }

  set coordMinMaxNodes(List<Node> nodes){
    nodes.forEach((n) {
      if(coordXMin > n.coordX)
        coordXMin = n.coordX;
      if(coordXMax < n.coordX)
        coordXMax = n.coordX;
      if(coordYMin > n.coordY)
        coordYMin = n.coordY;
      if(coordYMax < n.coordY)
        coordYMax = n.coordY;
    });
  }

  void add(Node n){
    nodes.add(n);
    num i = nodes.length-1;
    n.position = i;
    // update max/min here
  }

  void initValues(){
    maxDataBaseValue = 0.0;
    minDataBaseValue = 0.0;
    maxNodeValue = 0.0;
    minNodeValue = 0.0;
    maxAverageValue = 0.0;
  }

  void clear(){
    initValues();
    nodes.clear();
    nodesId2Pos.clear();
  }

  resetOrder() {
    nodes.sort((a, b) => a.id.compareTo(b.id)) ;
    resetPositions();
  }

  nodelinkOrder() {
    nodes.sort((b, a) => a.coordY.compareTo(b.coordY));
    resetPositions();
  }

  sortDegree(bool desc){
    if(desc)
      nodes.sort((a, b) => a.degree.compareTo(b.degree)) ;
    else
      nodes.sort((b, a) => a.degree.compareTo(b.degree)) ;

    resetPositions();
  }

  sortCommunity(bool desc){
    int compareComs (Node a, Node b) {
      if(!a.hasCommunity && b.hasCommunity) return 1;
      else if(a.hasCommunity && !b.hasCommunity) return -1;
      int result = (a.community).compareTo(b.community);
      if (result != 0) {
        return result;
      }

//      return result;
      result =  a.firstTs.compareTo(b.firstTs);

      if (result == 0){
        result = (b.degree).compareTo(a.degree);
      }
      return result;
    }
    if(desc)
      nodes.sort((a, b) => compareComs(a,b));
    else
      nodes.sort((b, a) => compareComs(a,b));

    resetPositions();
  }




  sortAlpha(bool desc){
    if(desc)
      nodes.sort((a, b) => a.name.compareTo(b.name)) ;
    else
      nodes.sort((b, a) => a.name.compareTo(b.name)) ;
    int pos = 0;
    this.valid.forEach((n){
      n.position = pos;
      pos++;
    });
  }

  sortTimeSlot(bool desc){
    int compareTs (Node a, Node b) {
      int result = b.firstTs.compareTo(a.firstTs);
      if (result != 0) {
        return result;
      }
      return (a.degree).compareTo(b.degree);
    }
    if(desc)
      nodes.sort((a, b) => compareTs(a, b)) ;
    else
      nodes.sort((b, a) =>  compareTs(a, b)) ;

    resetPositions();
  }

  orderElements(List<int> order){

//    if(order.length != nodes.length) print("¯\_(ツ)_/¯"); // TODO (Pao): raise exception

    int i = 0;
    nodes.forEach((n){
      nodesId2Pos[n.id] = i;
      n.visited = false;
      i++;
    });

    int pos = 0;
    order.forEach((o){
      Node n  = nodes[nodesId2Pos[o]];
      if(n.isValid) {
        n.position = pos;
        n.visited = true;
        pos++;
      }
    });

    this.valid.forEach((n){
      if(!n.visited) {
        n.position = pos;
        pos++;
        n.visited = true;
      }
    });

    nodes.sort((Node a, Node b) => a.position.compareTo(b.position));
  }

  void filterValid(function){

//    List<Node> newFilteredNodes = nodes.where((n)=>function(n));

    nodes.forEach((n){
      n.isValid = n.isValid && function(n);
    });
    resetPositions();

//    return newFilteredNodes;
  }

  void preFilterValid(function){

//    List<Node> newFilteredNodes = nodes.where((n)=>function(n));

    nodes.forEach((n){
      n.toBeValid = n.toBeValid && function(n);
    });
//    return newFilteredNodes;
  }

  void resetValid(){
    nodes.forEach((n){
      n.isValid = true;
    });
  }

  resetPositions(){
    int pos = 0;
    nodes.forEach((n){
      if(n.isValid || n.toBeValid) {
        n.position = pos;
        n.visited = true;
        pos++;
      }
    });
  }

  /** sets the overall min/max value of the dataset */
  void computeMinMaxDataset(String value){
    //finds min/max values of the dataset
    nodes.forEach((n){
      if(n.slm.getMinVal(value) < minDataBaseValue)
        minDataBaseValue = n.slm.getMinVal(value);

      if(maxDataBaseValue < n.slm.getMaxVal(value))
        maxDataBaseValue = n.slm.getMaxVal(value);
    });

    //TODO: better having the distinction between local and global min/max. Here Q&D solution
    nodes.forEach((n){
      n.slm.updateMinMaxSparklines(minDataBaseValue, maxDataBaseValue, value);
    });
  }

  void computeAggregateValues() {
    nodes.forEach((n) {
      n.computeAggregatedValue();
    });
  }

  num getHeightAll(){
//    num slHeight = 0;
//    nodes.forEach((n) {
//      slHeight = slHeight + (n._margin + n.height + n._margin);
//    });
    return nodes.length * 2 * nodes[0].height;
  }


  List<Node> searchbyName(String name){
    List<Node> coincidences = new List<Node>();
    nodes.forEach((Node n){
//      if(n.isValid) {
        if (n.isSimilar(name))
          coincidences.add(n);
//      }
    });
    return coincidences;
  }

}
