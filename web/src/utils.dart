library utils;

import 'package:color/color.dart'; // HSL color management, and others
import 'dart:math';
import 'dart:html';

enum shapes { rect, fillrect, circle, fillcircle, star, cross, triangle, diamondSquare, diamond, reverseTriangle }

enum ColorCodings {
  grayscale,
  fixed,
  hue,
  sequential,
  red_yellow,
  WavSet3,
  Communities
}

List<List<int>> Set3 = [
  [128, 177, 211],
  [141, 211, 199],
  [190, 186, 218],
  [179, 222, 105],
  [255, 255, 179],
  [252, 205, 229],
  [253, 180, 98],
  [251, 128, 114]
];
List<List<int>> Pastel1 = [
  [251, 180, 174],
  [179, 205, 227],
  [255, 255, 204],
  [222, 203, 228],
  [254, 217, 166],
  [204, 235, 197],
  [229, 216, 189]
];

//List<List<int> > Set1 = [
//  [228,26,28],
//  [255,127,0],
//  [55,126,184],
//  [77,175,74],
//  [152,78,163],
//  [255,255,51],
//  [166,86,40],
//  [227,26,28],
//  [253,191,111],
//  [202,178,214],
//  [106,61,154]
//];

//List<List<int> > Set1 = [
//  [228,26,28],
//  [55,126,184],
//  [255,127,0],
//  [77,175,74],
//  [152,78,163],
//  [255,255,51],
//  [166,86,40],
//  [247,129,191]
//];

//List<List<int> > Set1 = [
//[251,154,153],
//[166,206,227],
//[51,160,44],
//[31,120,180],
//[178,223,138],
//[227,26,28],
//[253,191,111],
//[255,127,0],
//[202,178,214],
//[106,61,154],
//[255,255,153],
//[177,89,40]];

List<List<int>> Set1 = [
  [55, 126, 184],
  [228, 26, 28],
  [77, 175, 74],
  [152, 78, 163],
  [255, 127, 0],
  [255, 255, 51],
  [247, 129, 191]
];

enum LineStyle { solid, dashed, dashedProportional }
enum EdgesOpt { packed }

enum SortingNodes {
  nodeId,
  alpha,
  degree,
  leafOrder,
  barycentric,
  spectralOrder,
  timeslot,
  community,
  rcm
}

enum buttonStatus { normal, pressed }

class ButtonOld {
  DivElement element;
  bool status;

  Button(DivElement de, bool pressed) {
    element = de;
    status = pressed;
  }
}

class Button {
  ButtonElement element;
  bool status;

  Button(ButtonElement de, bool pressed) {
    element = de;
    status = pressed;
  }
}

class Margin<T extends num> {
  final T top;
  final T right;
  final T bottom;
  final T left;

  const Margin(T t, T r, T b, T l)
      : this.top = t,
        this.right = r,
        this.bottom = b,
        this.left = l;

  String toString() => 'Margin($top, $right, $bottom, $left)';
}

/* returns a string representing the HEX color code according to the changing HUE value
*  parameters grayscale and colorCoding are optional and work for some visualizations
*  colorCoding: "divergent" in case there is a zero in the data and divergency is recommended
*  lightColor is used in case of overlapping vis, so the lightColor has a ligher luminance
*  and is better for background*/
String getColorCode(num min, num max, num value, ColorCodings colorCoding,
    bool lightColor, int baseHue, HexColor maxColor,
    [int i = null]) {
  String returnString = "#C0ffee";

  //consistency check in case min and max are inverted
  if (max < min) {
    double tempMin = min;
    min = max;
    max = tempMin;
  }

  double maxHSLColor = maxColor.toHslColor().l;
  double weight = maxHSLColor / (max - min);
  double luminanceWeight = maxHSLColor / (max - min);
//  double weight = 100 / (max - min);
//  double luminanceWeight = 100 / (max - min);

  if (lightColor) luminanceWeight = 100 / ((max * 1.8) - min);

  if (colorCoding != null) {
    switch (colorCoding) {
      case ColorCodings.grayscale:
        int lightnessValue =
        (maxHSLColor - (luminanceWeight * (value - min))).toInt();

        if (lightnessValue < 0)
          lightnessValue = 0; // fixes high precision data rounds
        if (lightnessValue > maxHSLColor)
          lightnessValue =
              maxHSLColor.toInt(); // fixes high precision data rounds

        //returnString = "#" + new HslColor(0, 0, lightnessValue).toHexColor().toString();
        returnString =
            "#" + new HslColor(0, 0, lightnessValue).toHexColor().toString();
        break;

      case ColorCodings.sequential:
        double hue = new RgbColor(49, 130, 189).toHslColor().h;
        if (baseHue != null) hue = baseHue.toDouble();
        int lightnessValue = (100 - (luminanceWeight * (value - min))).toInt();

        if (lightnessValue < 0)
          lightnessValue = 0; // fixes high precision data rounds
        if (lightnessValue > 100)
          lightnessValue = 100; // fixes high precision data rounds

        returnString = "#" +
            new HslColor(hue, 100, lightnessValue).toHexColor().toString();
        break;

      case ColorCodings.hue:
        weight = 240 / (max - min);
        returnString = "#" +
            new HslColor((240 - (weight * (value - min))).toInt(),
                lightColor ? 90 : 100, lightColor ? 70 : 50)
                .toHexColor()
                .toString();
        break;

      case ColorCodings.red_yellow:
        weight = 120 / (max - min);
        returnString = "#" +
            new HslColor((120 - (weight * (value - min))).toInt(),
                lightColor ? 90 : 100, lightColor ? 70 : 50)
                .toHexColor()
                .toString();
        break;

      case ColorCodings.fixed:
        double hue = new RgbColor(49, 130, 189).toHslColor().h;
        if (baseHue != null) hue = baseHue.toDouble();
        returnString = "#" +
            new HslColor(hue, 100, lightColor ? 70 : 50)
                .toHexColor()
                .toString();
        break;

      case ColorCodings.WavSet3:
        num lum = (value - min) / (max - min);
        HslColor color =
        new RgbColor(Set3[i][0], Set3[i][1], Set3[i][2]).toHslColor();

        returnString = "#" +
            new HslColor(color.h, color.s, 100 - (lum * (100 - color.l)))
                .toHexColor()
                .toString();
        break;

      case ColorCodings.Communities:
        int idx = value.toInt() % Set1.length;
        HslColor color =
        new RgbColor(Set1[idx][0], Set1[idx][1], Set1[idx][2]).toHslColor();

        returnString = "#" +
            new HslColor(color.h, color.s, lightColor ? 85 : color.l)
                .toHexColor()
                .toString();
        break;

    }
    return returnString;
  }
  return returnString;
}

/**
 *  utilities
 */
// p is inside a rectangle defined by corners <(x,y),(x2,y2)>?
bool inRect(Point p, num x, num y, num x2, num y2) =>
    (p.x >= x && p.x <= x2 && p.y >= y && p.y <= y2);

bool inVisibleCanvasArea(
    CanvasElement canvas, num objWidth, num objHeight, Point p) {
  Rectangle canvasRect = new Rectangle(
      -objWidth,
      -objHeight,
      canvas.width / window.devicePixelRatio + objWidth,
      canvas.height / window.devicePixelRatio + objHeight);
  return canvasRect.containsPoint(p);
}

bool objInRect(Rectangle rect, Rectangle objRect) {
//  print('rr');
//  print(rect.left);
//  print(rect.right);
//  print(rect.top);
//  print(rect.bottom);
//  print(objWidth);
//  print(objHeight);
  return rect.intersects(objRect);
//  return inRect(topleft, rect.left-objWidth, rect.top-objHeight, rect.right, rect.bottom);
}

bool inVisibleArea(Rectangle rect, Point p) {
  return rect.containsPoint(p);
}

num scaleCoord(num coord, num minOrig, num maxOrig, num minMap, num maxMap) {
  num xStd = (coord - minOrig) / (maxOrig - minOrig);
  return xStd * (maxMap - minMap) + minMap;

//    displacement + (n.coordX + nodes._displacementCoordX) * len / (nodes.coordXMax - nodes.coordXMin);
//  return minOrig +  (maxMap - minMap) * (coord+minOrig) / (maxOrig - minOrig);
}

// for test purposes it works only for vertical lines TODO: improve for any straight line
void customLineTo(CanvasRenderingContext2D context, Point start, Point end,
    LineStyle lineStyle,
    [num lineWidth = 1]) {
  switch (lineStyle) {
    case LineStyle.solid:
      var prevLinedash = context.getLineDash();
      context
        ..beginPath()
        ..moveTo(start.x, start.y)
        ..lineTo(end.x, end.y)
        ..closePath()
        ..stroke()
        ..setLineDash(prevLinedash);
      break;

//    case LineStyle.dashedProportional:
//      // get previous drawing styles
//      var prevLinedash = context.getLineDash();
//      var prevStrokeStyle = context.strokeStyle;
//
//      num factor = 2;
//      int space = 3;
//      num maxDist = 0;
//      Point currPoint = start;
//      context.lineWidth = lineWidth
//      ;
//      num distance = (end.y - start.y).abs();
//      maxDist = distance / 4; //empirically computed
//      while ((distance > space) && maxDist > space) {
//        distance = distance / factor;
//        distance = (distance > maxDist) ? maxDist : distance;
//        context
//          ..beginPath()
//          ..moveTo(currPoint.x, currPoint.y)
//          ..lineTo(currPoint.x, currPoint.y + distance - space)
//          ..stroke();
//        currPoint = new Point(currPoint.x, currPoint.y + distance);
//        distance = (end.y - currPoint.y).abs();
//      }
//
//      context
//        ..beginPath()
//        ..moveTo(currPoint.x, currPoint.y)
//        ..setLineDash([2, 2])
//        ..lineTo(end.x, end.y)
//        ..closePath()
//        ..stroke()
//        ..strokeStyle = prevStrokeStyle
//        ..setLineDash(prevLinedash);
//      break;

    case LineStyle
        .dashedProportional: // assumes only vertical lines, TODO: modify also for other lines

      num factor = 1 / 13;
      List lineDistribution = [
        0,
        factor,
        factor * 2,
        factor * 4,
        factor * 5,
        factor * 8,
        factor * 9,
        factor * 11,
        factor * 12,
        factor * 13
      ];
      num distance = (end.y - start.y).abs();
      context.lineWidth = lineWidth;
      for (int i = 0; i < (lineDistribution.length / 2); i++) {
        context
          ..beginPath()
          ..moveTo(start.x, start.y + lineDistribution[2 * i] * distance)
          ..lineTo(start.x, start.y + lineDistribution[2 * i + 1] * distance)
          ..stroke();
      }
      break;

    case LineStyle.dashed:
      var prevLinedash = context.getLineDash();
      num distance = (end.y - start.y).abs();
      num dashProportion = distance / 5;
      num spaceProportion = dashProportion / 2;
//      spaceProportion = (spaceProportion > 40) ? 40: spaceProportion;
      context
        ..beginPath()
        ..setLineDash([dashProportion, spaceProportion])
        ..moveTo(start.x, start.y)
        ..lineTo(end.x, end.y)
        ..closePath()
        ..stroke()
        ..setLineDash(prevLinedash);
      break;
  }
}

// for test purposes it works only for vertical lines TODO: improve for any straight line
void customLineWithNegativeTo(CanvasRenderingContext2D context, Point start,
    Point end, LineStyle lineStyle, String color1,
    [String color2, num lineWidth = 1]) {
//  //TODO: remove or comment the following
//  print("entered in customLineWithNegativeTo function");

  // get previous drawing styles
//  var prevLinedash = context.getLineDash();
//  var prevStrokeStyle = context.strokeStyle;
  List<num> prevLinedash = context.getLineDash();
  var prevStrokeStyle = context.strokeStyle;
  context.lineWidth = lineWidth;

//  //TODO: remove or comment the following
//  print("before switching lineStyle, which has value: " + lineStyle.toString());

  switch (lineStyle) {
    case LineStyle.solid:
//  //TODO: remove or comment the following
//    print("lineStyle is solid, current context.strokestyle is: " + context.strokeStyle.toString());

      context.strokeStyle = color1;
      context
        ..beginPath()
        ..moveTo(start.x, start.y)
        ..lineTo(end.x, end.y)
        ..closePath()
        ..stroke();
//    //TODO: remove or comment the following
//    print("the line has been correctly drawn");

      break;

    case LineStyle
        .dashedProportional: // assumes only vertical lines, TODO: modify also for other lines

    //first the gray line then dashes
      context.strokeStyle = color2;
      context
        ..beginPath()
        ..moveTo(start.x, start.y)
        ..lineTo(start.x, end.y)
        ..stroke();

      //TODO: make lineDistribution parametric
      // use a cycle to assign weights (factor*i) where i is the incremental variable of the cycle
      num factor = 1 / 13;
      List lineDistribution = [
        0,
        factor,
        factor * 2,
        factor * 4,
        factor * 5,
        factor * 8,
        factor * 9,
        factor * 11,
        factor * 12,
        factor * 13
      ];
      num distance = (end.y - start.y).abs();

      context.strokeStyle = color1;
      context.beginPath();
      for (int i = 0; i < lineDistribution.length - 1; i += 2) {
        context
          ..moveTo(start.x, start.y + lineDistribution[i] * distance)
          ..lineTo(start.x, start.y + lineDistribution[i + 1] * distance);
      }
      context.stroke();

      break;

    case LineStyle.dashed:
    //first one gray line then dashes
      context.strokeStyle = color2;
      context.beginPath();
      context
        ..moveTo(start.x, start.y) // + dashProportion*i)
        ..lineTo(end.x, end.y) // + dashProportion*(i+1))
        ..stroke();

      num distance = (end.y - start.y).abs();
      num divs = 9.0;
      num dashProportion = distance / divs;

      context.strokeStyle = color1;
      context.beginPath();
      for (int i = 0; i < divs; i += 2) {
        context
          ..moveTo(start.x, start.y + dashProportion * i)
          ..lineTo(start.x, start.y + dashProportion * (i + 1));
        context.stroke();
      }
      context.stroke();

      break;
  }

  //TODO: check wheter the following instruction is useful or not, it causes problems in javascript conversion
  //context.setLineDash(prevLinedash);
  context.strokeStyle = prevStrokeStyle;

//  // get previous drawing styles
//  var prevLinedash = context.getLineDash();
//  var prevStrokeStyle = context.strokeStyle;
}

class Line {
  num X1;
  num Y1;
  num X2;
  num Y2;

  Line(Point p1, Point p2) {
    X1 = p1.x;
    Y1 = p1.y;
    X2 = p2.x;
    Y2 = p2.y;
  }
}

/// <summary>
/// This is based off an explanation and expanded math presented by Paul Bourke:
///
/// It takes two lines as inputs and returns true if they intersect, false if they
/// don't.
/// If they do, ptIntersection returns the point where the two lines intersect.
/// </summary>
/// <param name="L1">The first line</param>
/// <param name="L2">The second line</param>
/// <param name="ptIntersection">The point where both lines intersect (if they do).</param>
/// <returns></returns>
/// <remarks>See http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/</remarks>
Point getLinesIntersect(Line L1, Line L2) {
// Denominator for ua and ub are the same, so store this calculation
  double d =
      (L2.Y2 - L2.Y1) * (L1.X2 - L1.X1) - (L2.X2 - L2.X1) * (L1.Y2 - L1.Y1);

//n_a and n_b are calculated as seperate values for readability
  double n_a =
      (L2.X2 - L2.X1) * (L1.Y1 - L2.Y1) - (L2.Y2 - L2.Y1) * (L1.X1 - L2.X1);
  double n_b =
      (L1.X2 - L1.X1) * (L1.Y1 - L2.Y1) - (L1.Y2 - L1.Y1) * (L1.X1 - L2.X1);

// Make sure there is not a division by zero - this also indicates that
// the lines are parallel.
// If n_a and n_b were both equal to zero the lines would be on top of each
// other (coincidental).  This check is not done because it is not
// necessary for this implementation (the parallel check accounts for this).
  if (d == 0) return new Point(-1, -1);

// Calculate the intermediate fractional point that the lines potentially intersect.
  double ua = n_a / d;
  double ub = n_b / d;

// The fractional point will be between 0 and 1 inclusive if the lines
// intersect.  If the fractional calculation is larger than 1 or smaller
// than 0 the lines would need to be longer to intersect.
  if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0) {
    Point ptIntersection = new Point(
        L1.X1 + (ua * (L1.X2 - L1.X1)), (L1.Y1 + (ua * (L1.Y2 - L1.Y1))));
    return ptIntersection;
  }
  return new Point(-1, -1);
}

/// draws a shape on a canvas
/// set the drawing style before calling this function
/// size correspond to the radius, in case of a circle or
/// to half edge, in case of a rectangle
void drawVertex(
    CanvasRenderingContext2D ctx, Point center, num size, shapes shape) {
  switch (shape) {
    case shapes.circle:
      ctx
        ..beginPath()
        ..arc(center.x, center.y, size, 0, pi * 2, false)
        ..fillStyle = "black"
        ..fill()
        ..stroke();
      break;
    case shapes.fillcircle:
      ctx
        ..beginPath()
        ..arc(center.x, center.y, size, 0, pi * 2, false)
        ..fill()
        ..stroke();
      break;
    case shapes.rect:
      ctx
        ..beginPath()
        ..rect(center.x - size, center.y - size, size * 2, size * 2)
        ..fillStyle ="black"
        ..fill()
        ..stroke();
      break;
    case shapes.fillrect:
      ctx
        ..beginPath()
        ..fillRect(center.x - size, center.y - size, size * 2, size * 2)
        ..stroke()
        ..fill() //TODO: check this
          ;
      break;
    case shapes.star:
      ctx
        ..beginPath()
        ..moveTo(center.x, center.y-size)
        ..lineTo(center.x-(size*.9), center.y+size)
        ..lineTo(center.x+size, center.y-(size*.25))
        ..lineTo(center.x-size, center.y-(size*.25))
        ..lineTo(center.x+(size*.9), center.y+size)
        ..lineTo(center.x, center.y-size)
        ..stroke()
        ;
      break;
    case shapes.cross:
      ctx
        ..beginPath()
        ..moveTo(center.x-(size/2), center.y-size)
        ..lineTo(center.x+(size/2), center.y-size)
        ..lineTo(center.x+(size/2), center.y-(size/2))
        ..lineTo(center.x+size, center.y-(size/2))
        ..lineTo(center.x+size, center.y+(size/2))
        ..lineTo(center.x+(size/2), center.y+(size/2))
        ..lineTo(center.x+(size/2), center.y+size)
        ..lineTo(center.x-(size/2), center.y+size)
        ..lineTo(center.x-(size/2), center.y+(size/2))
        ..lineTo(center.x-size, center.y+(size/2))
        ..lineTo(center.x-size, center.y-(size/2))
        ..lineTo(center.x-(size/2), center.y-(size/2))
        ..lineTo(center.x-(size/2), center.y-size)
        ..fillStyle = "black"
        ..fill()
        ..stroke()
      ;
      break;

    case shapes.triangle:
      ctx
        ..beginPath()
        ..moveTo(center.x, center.y-size)
        ..lineTo(center.x-(size*.9), center.y+size)
        ..lineTo(center.x+(size*.9), center.y+size)
        ..lineTo(center.x, center.y-size)
        ..fillStyle = "black"
        ..fill()
        ..stroke()
      ;
      break;

    case shapes.diamondSquare:
      ctx
        ..beginPath()
        ..moveTo(center.x-size, center.y-size)
        ..lineTo(center.x+size, center.y-size)
        ..lineTo(center.x+size, center.y)
        ..lineTo(center.x, center.y+size)
        ..lineTo(center.x-size, center.y)
        ..lineTo(center.x-size, center.y-size)
        ..fillStyle = "black"
        ..fill()
        ..stroke()
      ;
      break;

    case shapes.diamond:
      ctx
        ..beginPath()
        ..moveTo(center.x, center.y-size)
        ..lineTo(center.x+size, center.y)
        ..lineTo(center.x, center.y+size)
        ..lineTo(center.x-size, center.y)
        ..lineTo(center.x, center.y-size)
        ..fillStyle = "black"
        ..fill()
        ..stroke()
      ;
      break;
    case shapes.reverseTriangle:
      ctx
        ..beginPath()
        ..moveTo(center.x-size, center.y-size)
        ..lineTo(center.x+size, center.y-size)
        ..lineTo(center.x, center.y+size)
        ..lineTo(center.x-size, center.y-size)
        ..fillStyle = "black"
        ..fill()
        ..stroke()
      ;
      break;
  }
}

/// draws a shape on a canvas
void drawCircle(
    CanvasRenderingContext2D ctx, Point center, num radius, String color) {
//  num previousGlobalAlpha = ctx.globalAlpha;

//  ctx.fillStyle = previousGlobalAlpha;
  ctx
    ..strokeStyle = color
    ..fillStyle = '#000000'
    ..lineWidth = 1
//    ..globalAlpha = 1
    ..beginPath();

  drawVertex(ctx, center, radius, shapes.fillcircle);

//  ctx
//    ..arc(center.x, center.y, radius, 0, PI*2, false)
//    ..fill()
//    ..stroke();

//  ctx.globalAlpha = previousGlobalAlpha;
}

ImageElement createImage(
    List<num> timeSeries, num min, num max, colorMap, bool lightColor) {
  String colorCode;

  CanvasElement canvasUtil = new CanvasElement();
  canvasUtil.height = 1;
  canvasUtil.width = timeSeries.length;
  CanvasRenderingContext2D ctxUtil = canvasUtil.getContext('2d');

  ImageData imageData = ctxUtil.createImageData(timeSeries.length, 1);

  int j = 0;
  for (int i = 0; i < timeSeries.length; i++) {
    //color //TODO: uncomment for time series!!
//    colorCode  = getColorCode(
//        min,
//        max,
//        timeSeries[i],
//        colorMap,
//        lightColor,
//        120,
//        new HexColor("#eeeeee"),
//        i );
    colorCode = getColorCode(1.0, 1.0, timeSeries[i], ColorCodings.Communities,
        lightColor, 120, new HexColor("#ffffff"), i);

    RgbColor rgbColor = new HexColor(colorCode).toRgbColor();

    imageData.data[j + 0] = rgbColor.r;
    imageData.data[j + 1] = rgbColor.g;
    imageData.data[j + 2] = rgbColor.b;
    imageData.data[j + 3] = 150;

    j += 4;
  }

  ctxUtil.putImageData(imageData, 0, 0);

  ImageElement img = document.createElement("img");
  img.src = canvasUtil.toDataUrl("image/png");
  return img;
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.parse(s, (e) => null) != null;
}
