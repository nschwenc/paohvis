

library paohvis;


import 'dart:js';
import 'dart:html';
import 'dart:math';
import 'dart:collection';
import 'dart:convert';// show JSON; // to parse json files
import 'dart:async';

import 'package:color/color.dart'; // HSL color management, and others
import 'package:collection/collection.dart';
import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';
import 'package:dnd/dnd.dart';

import 'lib/FastBitSet.dart';

import 'src/events/events.dart';
import 'globals.dart';

import 'src/theme.dart';
import 'src/utils.dart';

part 'src/graph/edge.dart';
part 'src/graph/node.dart';
part 'src/graph/nodes.dart';
part 'src/graph/graph.dart';
part 'src/graph/communities.dart';
part 'src/writer.dart';
part 'src/reader.dart';
part 'src/select_statistic_manager.dart';
part 'src/sparkline.dart';
part 'src/sparkline_manager.dart';
part 'src/edge_manager.dart';
part 'src/node_manager.dart';
part 'src/vis_manager.dart';
part 'src/time_labels.dart';
part 'src/scroll.dart';
part 'src/time_manager.dart';
part 'src/color_manager.dart';
part 'src/stats.dart';
part 'src/tabs.dart';
part 'src/checkbox.dart';
part 'src/minimize.dart';
part 'src/size_manager.dart';
part 'src/time_slider_manager.dart';
part 'src/community_legend_manager.dart';
part 'src/tooltip.dart';
part 'src/paoh_tool.dart';
part 'bmc2pao.dart';



/// Connection with the html file
///
///
DivElement _visDiv = document.querySelector('#visCanvas');

CanvasElement canvas = document.querySelector('#workspace');
CanvasElement canvasOverlay = document.querySelector('#overlay');
CanvasRenderingContext2D canvasContext = canvas.getContext('2d');

DivElement horScrollDiv = document.querySelector('#visScrollHor');
DivElement vertScrollDiv = document.querySelector('#visScrollVert');

CanvasElement horScrollCanvas = document.querySelector('#horizontal');
CanvasElement vertScrollCanvas = document.querySelector('#vertical');

ButtonElement _btnCurves = document.querySelector("#btncurvesbsp");
ButtonElement _btnEdgeSplat = document.querySelector("#btnsplatbsp");
ButtonElement _btnBioFabric = document.querySelector("#btnbiofabricbsp");
ButtonElement _btnNodeLink = document.querySelector("#btnnodelinksp");
ButtonElement _btnHeatMap = document.querySelector("#btnheatmapbsp");
ButtonElement _btnLineGraph = document.querySelector("#btnlinegraphbsp");
ButtonElement _btnInterleaving = document.querySelector("#btnInterleaving");
ButtonElement _btnNodeLabels = document.querySelector("#btnlaberlsbsp");
ButtonElement _btnHighlight = document.querySelector("#btnhighlightbsp");
ButtonElement _btnSelection = document.querySelector("#btnselectbsp");
LIElement _highlightGroup = document.querySelector("#highlightGroup");
LIElement _selectGroup = document.querySelector("#selectGroup");
ButtonElement _btnFilter = document.querySelector("#btnfiltertbsp");
ButtonElement _btnFilterRemove = document.querySelector("#btnfilterremove");
ButtonElement _btnReset = document.querySelector("#btnresettbsp");
ButtonElement _btnBindNL = document.querySelector("#btnbindNLtbsp");
ButtonElement _btnTooltip = document.querySelector("#btnToolTip");
ButtonElement _btnRole = document.querySelector("#btnRole");
ButtonElement _btnColorGroup = document.querySelector("#btnColorGroup");
ButtonElement _btnEdgePacking = document.querySelector("#btnEdgePacking");
ButtonElement _btnAlternateColors = document.querySelector("#btnAlternateColors");
//ButtonElement _btnSortEdges = document.querySelector("#btnSortEdges");

ButtonElement _btnRepeatArch = document.querySelector("#btnRepeatArch");
ButtonElement _btnShowHyper = document.querySelector("#btnShowHyper");

AnchorElement _saveCanvas = document.querySelector("#anchorSaveCanvas");
AnchorElement _anchorSave = document.querySelector("#anchorSave");
FileUploadInputElement _inputFileOpen = document.querySelector("#inputFileOpen");

NumberInputElement _filterDegree = document.querySelector("#filterDegree");
TextInputElement _searchName = document.querySelector("#searchName");
ButtonElement _btnSearchName = document.querySelector("#btnSearchName");

RangeInputElement _rngIntensity = document.querySelector("#rngIntensity");

RangeInputElement _tsWidthSlider = document.querySelector("#tsWidthSlider");
SpanElement _decreaseTsWidth = document.querySelector("#ts-decrease");
SpanElement _increaseTsWidth = document.querySelector("#ts-increase");


//RangeInputElement _sliderZoom = document.querySelector("#sliderZoom");
//SpanElement _decreaseZoom = document.querySelector("#zoom-decrease");
//SpanElement _increaseZoom = document.querySelector("#zoom-increase");

RangeInputElement _sliderZoomNode = document.querySelector("#sliderZoomNode");
SpanElement _decreaseZoomNode = document.querySelector("#node-decrease");
SpanElement _increaseZoomNode = document.querySelector("#node-increase");


ButtonElement _fitZoomTs = document.querySelector("#zoom-fit-ts");
ButtonElement _fitZoomNode = document.querySelector("#zoom-fit-node");

ButtonElement _btnToolbarVisible = document.querySelector("#btn-toolbar-visibility");
SpanElement _iconToogleVisibility = document.querySelector("#icon-toogle-visibility");
ElementList<DivElement> _secondaryRows = document.querySelectorAll(".secondary-rows");


DivElement _ttip = document.querySelector("#tltip");
DivElement _divLegend = document.querySelector("#divCommunityLegend");

LIElement _divDsDescription = document.querySelector("#dsDescription");

//DivElement _tlbar = document.querySelector("#tlbar");

SelectElement _selectEdgesOrder = document.querySelector("#orderEdgesSelect_");


SelectElement _selectOrder = document.querySelector("#orderSelect");
SelectElement _selectSymbol = document.querySelector("#symbolSelect");

DivElement _stElement = document.querySelector('#tsStatistics');
SelectElement _selectStElement = document.querySelector('#selectTsStatistic');
DivElement _stElementLeft = document.querySelector('#tsStatisticsLeft');
SelectElement _selectStElementLeft = document.querySelector('#selectTsStatisticLeft');

DivElement _sideNav = document.querySelector("#sideNav");
AnchorElement _sideNavButton = document.querySelector("#sideNavButton");
AnchorElement _sideNavClose = document.querySelector("#sideNavClose");
DivElement _sideNavContent = document.querySelector("#sideNavContent");



//List reordering = [
//  "#btnOrd" + SortingNodes.nodeId.index.toString(),
//  "#btnOrd" + SortingNodes.alpha.index.toString(),
//  "#btnOrd" + SortingNodes.degree.index.toString(),
//  "#btnOrd" + SortingNodes.leafOrder.index.toString(),
//  "#btnOrd" + SortingNodes.barycentric.index.toString(),
//  "#btnOrd" + SortingNodes.spectralOrder.index.toString(),
//];

/// list for managing arrays of buttons instead of selecting one by one
List linestyle = [
  "#btnSty" + LineStyle.solid.index.toString(),
  "#btnSty" + LineStyle.dashed.index.toString(),
  "#btnSty" + LineStyle.dashedProportional.index.toString(),
];


CheckboxInputElement _checkNodeColorAsEdge = document.querySelector('#nodeColorAsEdge');
CheckboxInputElement _checkHyperedgesStronger = document.querySelector('#hyperedgesStronger');
CheckboxInputElement _checkHyperedgesSplat = document.querySelector('#hyperedgesSplat');
CheckboxInputElement _checkShowDegree = document.querySelector('#showDegree');
CheckboxInputElement _checkAndSelection = document.querySelector('#andSelection');
CheckboxInputElement _checkShowHighlightSelect = document.querySelector('#showhighlightselect');
CheckboxInputElement _checkHideNotImportant = document.querySelector('#hideNotImportant');

List<Button> orderingList = new List<Button>();
List<Button> lineStylingList = new List<Button>();

/// Files and folders
String _jsonFileTheme;
String _jsonDBList;
String _serverBasePath;
String _dataBasePath;
//String _jsonDB;

/// Theme panel
List<OptionElement> _opList;
SelectElement _selectTheme;
List listTheme = new List();
String selectedTheme = "";
String currentTheme = "";

/// DB panel
List<OptionElement> _opDBList;
SelectElement _selectDBFile;
List listDBFiles = new List();
String selectedDB = "";
String currentDB = "";
bool storedOK = false; // if true the user saved the preferences

/**
 * Performs some check in order to be sure that the Web UI is
 * consistent with the internal state of the application
 * Specifically, it checks if global variable are consistent with
 * the status declared in the UI
 * */
void checkUIIntegrity(){
  changePressedButtonStyleBootstrap (_btnCurves, CURVES);
  changePressedButtonStyleBootstrap (_btnBioFabric, PAOVIS);
  changePressedButtonStyleBootstrap (_btnEdgeSplat, SPLAT);
  changePressedButtonStyleBootstrap (_btnNodeLink, NODELINK);

  changePressedButtonStyleBootstrap (_btnShowHyper, SHOW_HYPERGRAPH);

  changePressedButtonStyleBootstrap (_btnHeatMap, HEATMAP);
  changePressedButtonStyleBootstrap (_btnLineGraph, LINEGRAPH);
  changePressedButtonStyleBootstrap (_btnInterleaving, SPLAT_INTERLEAVING);

  changeActiveButtonStyleBootstrap (_btnInterleaving, SPLAT);
//  changeActiveButtonStyleBootstrap (_btnSortEdges, PAOVIS);
  changeActiveButtonStyleBootstrap (_btnEdgePacking, PAOVIS);
  changeActiveButtonStyleBootstrap (_btnRepeatArch, PAOVIS);  //repeat
  changeActiveButtonStyleBootstrap (_btnBindNL, NODELINK);

  changeActiveButtonStyleBootstrap (_btnFilter, NODE_SELECTED||EDGE_SELECTED||TS_SELECTED);
  changeActiveButtonStyleBootstrap (_btnFilterRemove, NODE_SELECTED||EDGE_SELECTED||TS_SELECTED);

  changePressedButtonStyleBootstrap (_btnHighlight, ENABLED_HIGHLIGHT);
  changePressedButtonStyleBootstrap (_btnSelection, ENABLED_SELECTION);
  changePressedButtonStyleBootstrap (_btnBindNL, ENABLED_BIND_NODELINK);
  changePressedButtonStyleBootstrap (_btnFilterRemove, SHOW_CONTEXT && (NODE_SELECTED||EDGE_SELECTED||TS_SELECTED));


  changePressedButtonStyleBootstrap (_btnNodeLabels, NODE_LABELS);
  changePressedButtonStyleBootstrap (_btnTooltip, ENABLED_TOOLTIP);
  changePressedButtonStyleBootstrap (_btnRole, ENABLED_NODEROLE);
  changePressedButtonStyleBootstrap (_btnColorGroup, ENABLED_COLORGROUP);

  changePressedButtonStyleBootstrap (_btnAlternateColors, ALTERNATE_COLORS);
  changePressedButtonStyleBootstrap (_btnEdgePacking, HYPENET_PACK_EDGES);
  changePressedButtonStyleBootstrap (_btnRepeatArch, REPEAT); //repeat
//  changePressedButtonStyleBootstrap (_btnSortEdges, HYPENET_SORT_LENGTH);

  // changePressedButtonStyle (_btnBioFabric, BIOFABRIC);
//  changePressedButtonStyle (_btnEdgeSplat, SPLAT);
//  changePressedButtonStyle (_btnHeatMap, HEATMAP);
//  changePressedButtonStyle (_btnLineGraph, LINEGRAPH);
//  changePressedButtonStyle (_btnNodeLabels, NODE_LABELS);
//  changePressedButtonStyle (_btnSelection, ENABLED_SELECTION);
//  changePressedButtonStyle (_btnHighlight, ENABLED_HIGHLIGHT);


  for (int i = 0; i < LineStyle.values.length; i++){
    changePressedButtonStyleBootstrap(lineStylingList[i].element, lineStylingList[i].status);
  }

  changeCheckedInputStyleBootstrap(_checkNodeColorAsEdge, NODE_COLOR_AS_EDGE);
  changeCheckedInputStyleBootstrap(_checkHyperedgesSplat, HYPER_EDGES_SPLAT);
  changeCheckedInputStyleBootstrap(_checkHyperedgesStronger, HYPER_EDGES_STRONGER);
  changeCheckedInputStyleBootstrap(_checkShowDegree, SHOW_NODE_DEGREE);
  changeCheckedInputStyleBootstrap(_checkAndSelection, AND_SELECTIONS_FILTERS);
  changeCheckedInputStyleBootstrap(_checkShowHighlightSelect, ENABLED_HIGHLIGHT_AND_SELECTIONS);
  changeCheckedInputStyleBootstrap(_checkHideNotImportant, ENABLED_NOT_IMPORTANT_BUTTONS);


  changeVisibilityButtonStyleBootstrap(ENABLED_HIGHLIGHT_AND_SELECTIONS);
  changeVisibilityNotImportantButtonStyleBootstrap(ENABLED_NOT_IMPORTANT_BUTTONS);


//  for (int i = 0; i < SortingNodes.values.length; i++){
//    changePressedButtonStyleBootstrap(orderingList[i].element, orderingList[i].status);
//  }

}

void changeVisibilityButtonStyleBootstrap(bool isVisible){

  var visibility = "display: none";
  if (isVisible){
    visibility = "display: inline";
  }
  _highlightGroup.setAttribute("style", visibility);
  _selectGroup.setAttribute("style", visibility);
}

void changeVisibilityNotImportantButtonStyleBootstrap(bool isVisible){

  ElementList<LIElement> notImportantIcons = document.querySelectorAll('.icon-not-important');

  var visibility = "display: none;";
  if (isVisible){
    visibility = "display: inline-block;";
  }

  notImportantIcons.forEach((LIElement de){
    de.setAttribute("style", visibility);
  });

}

///**
// * Takes care of button appearance on the screen
// * */
//void changePressedButtonStyle (DivElement elem, bool isPressed){
//  if(isPressed)
//    elem.setAttribute("class", "usefulButton usefulButtonPressed");
//  else
//    elem.setAttribute("class", "usefulButton usefulButtonReleased");
//}

/**
 * Takes care of button appearance on the screen
 * */
void changePressedButtonStyleBootstrap (ButtonElement elem, bool isPressed){
  if(isPressed)
    elem.setAttribute("class", "btn btn-success active");
  else
    elem.setAttribute("class", "btn btn-default");
}

void changeActiveButtonStyleBootstrap (ButtonElement elem, bool isActive){
  elem.disabled = !isActive;
}

void changeCheckedInputStyleBootstrap (CheckboxInputElement elem, bool isChecked){
  elem.checked = isChecked;
}


RangeInputElement _timeSlider = document.querySelector("#timeSlider");
DivElement _timeSliderLables = document.querySelector("#timeSliderLabels");
SpanElement _tsLabelMin = document.querySelector("#tl-min");
SpanElement _tsLabelMax = document.querySelector("#tl-max");


// entry point
void main() {

  _serverBasePath = SERVER_BASE_PATH;
  _dataBasePath = DATA_BASE_PATH;
  _jsonFileTheme = JSON_FILE_THEME;
  _jsonDBList = JSON_DB_LIST;

  Paohvis paohvis = new Paohvis();
  checkUIIntegrity();
  paohvis.setBasePath(_serverBasePath); // specify files location
  paohvis.setDataPath(_dataBasePath); // specify files location
  paohvis.setSelectTheme(querySelector('body').querySelector("#selectTheme"));
  paohvis.setSelectDBFile(querySelector('body').querySelector("#selectFile"));
  paohvis.loadThemeName(_serverBasePath + "/" + _jsonFileTheme);
  paohvis.loadDBList(_serverBasePath + "/" + _jsonDBList); // sets selectedDB
  paohvis.setupVis();
}

