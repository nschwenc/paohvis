library globals;

import 'dart:math';

final bool DEBUG = false;

final String SERVER_BASE_PATH = "data";
final String DATA_BASE_PATH = "data";
final String JSON_FILE_THEME = "themes.json";
final String JSON_DB_LIST = "dbfilenames_.json";

final String CANVAS_FONT = "Source Sans Pro";

bool SHOW_TABS = false; // tabs to minimize and select TSs
bool ENABLED_NOT_IMPORTANT_BUTTONS = false; // true: all buttons are in toolbar

bool PAOVIS = true;
bool SPLAT = false;
bool CURVES = false;
bool NODELINK = false;

bool HEATMAP = false;
bool HEATMAP_IMG = true;
bool LINEGRAPH = false; //new
bool DRAW_SQUARE = false;

bool SPLAT_INTERLEAVING = false; //new

bool REPEAT = false; //new
bool HYPENET_PACK_EDGES = false; //new
bool HYPENET_SORT_LENGTH = false; //new

bool NODELINK_ENABLED_EXPANDNL = false;

bool MAXVALUEVISIBLE = false;
bool NODE_LABELS = true;
bool NODE_AGGREGATED = true;
bool TIME_LABELS = true;
bool ENABLED_HIGHLIGHT = true;
bool ENABLED_SELECTION = true;
bool AND_SELECTIONS_FILTERS = true;
bool ENABLED_FILTER = false;
bool ENABLED_BIND_NODELINK = false;
bool ENABLED_TOOLTIP = false;
bool ENABLED_NODEROLE = false;
bool ENABLED_COLORGROUP = false;
bool HISTOGRAM_LINEGRAPH = true;
bool INTENSITY_CHANGING = false; //new
bool RANGE_NODELINK_CHANGING = false; //new
bool SLIDER_NODELINK_MOVED = false;

bool ALTERNATE_COLORS = false;

bool ENABLED_HIGHLIGHT_AND_SELECTIONS = false; // new

int FILTER_DEGREE = 2;
bool GHOST_TOOLTIPS = true;

bool SHOW_HYPERGRAPH = true;
bool SHOW_NODE_DEGREE = false;

bool HYPER_EDGES_STRONGER = false;
bool HYPER_EDGES_SPLAT = false;

bool NODE_COLOR_AS_EDGE = false;

bool NODE_HIGHLIGHTED = false;
bool DRAW_BAR = false;
bool NODE_SELECTED = false;

bool EDGE_HIGHLIGHTED = false;
bool EDGE_SELECTED = false;

bool TS_HIGHLIGHTED = false;
bool TS_SELECTED = false;

bool FILTERING_TRANSITION = false;

bool SHOW_CONTEXT = true;

int SYMBOL_SELECTED = 0;

//List<String> ALT_COLORS = ["#fff0f0", "#ffffff"];
//List<String> ALT_COLORS = ["#000000", "#ffffff"];
String COLOR_LINES_BACK = "#cccccc";

const PI2 = 2 * pi;
const MAX_FONT = 24;
const LINE_WIDTH = 1.7;
const LINE_WIDTH_STRONG = 1.5;
const LINE_WIDTH_HIGHLIGHT = 1.5;
const NODE_RADIUS = 1.1;
const NODE_HEIGHT_RAT = 0.7;
const MIN_NODE_RADIUS = 2.1;
const MAX_NODE_RADIUS = 7.5;
const EDGE_SEP = 1.8;
const TIME_SLOT_SEP = 4.0;
const MIN_TS_WIDTH = 40.0;

// interaction vars
num NODE_HEIGHT_DIM = 6.0;
num SCALE = 2.0;
num BACKGROUND_INTENSITY = 0.95;
num TS_WIDTH = 0;
num NODES_FRAME_WIDTH = MIN_TS_WIDTH;

int N_ALTERNATE_ROWS = 1;


String URL_RESPONSE = 'http://localhost:5000/response/';

int CANVAS_WIDTH = 3000;
int CANVAS_HEIGHT = 1500;

bool DEMO_VIS4DH = false;
bool RUNNING_EXP = false;
bool EXPERIMENT = false;