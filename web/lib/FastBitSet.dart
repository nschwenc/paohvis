@JS()
library fastbitset;

import 'package:js/js.dart';

@JS()
abstract class FastBitSet {
  external factory FastBitSet(List<int> _);
  external add(int);
  external bool has(int);
  external intersects(FastBitSet);
  external int intersection_size(FastBitSet);
  external int size();
  external void clear();
}
