part of paohvis;

class Bmc2pao {

  Bmc2pao(String jsonString) {
    loadFileMB(jsonString);//"data/MarieBoucher-hypergraph-draft.json");
  }

  loadFileMB(String jsonString) async {
    try {
      var data = await HttpRequest.getString(jsonString); // sends HTTP request of specified path
      _getMBDataFromJsonFile(data); // calls _getDataFromFile if the request was successful
    } catch (e) {
      handleError(e);
    }
  }

  void _getMBDataFromJsonFile(String jsonString) {
    Map fileData = json.decode(jsonString); // gets the JSON file
    // reset to make sure graph is empty: we are about to load a new graph

    //get metadata
    var metadata = fileData['metadata'];
    //check if file is the new format
      _parseFileFormat(fileData);
  }


  void _parseFileFormat(Map fileData) {
    var edgesInFile = fileData['edges'];
    Map nodes = {};
    Map edges = new Map();
    var length = 0;

    edgesInFile.forEach((ef) {
      if (nodes.isNotEmpty) length = nodes.length;
      nodes.putIfAbsent(ef['Nom1'].toString(), () => length);
    });

    length = nodes.length;
    print("$nodes inserted");
  }

  /**
   * Method that should manage errors
   */
  void handleError(var error){
    print('Error in paoh_tool.dart ...');
    print(error);
  }
}